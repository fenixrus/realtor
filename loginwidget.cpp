#include "loginwidget.h"
#include "ui_loginwidget.h"

LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWidget)
{                                                                                      //  созд об-т формы авт-и с полями лог и пароль
    ui->setupUi(this);
    connect(this,SIGNAL(wrongAcc()),
            this,SLOT(slotWrongAcc()));
    connect(ui->buttonAuth, SIGNAL(clicked()),
            this,SIGNAL(loginAccepted()));
    connect(ui->inputLogin,SIGNAL(textEdited(QString)),
            this, SLOT(checkNotEmptyForm()));
    connect(ui->inputPassword,SIGNAL(textEdited(QString)),
            this, SLOT(checkNotEmptyForm()));
    ui->buttonAuth->setEnabled(false);
    ui->inputLogin->setValidator(new QRegExpValidator(QRegExp ("[a-zA-Z0-9]{3,15}"),this));
    ui->inputPassword->setValidator(new QRegExpValidator(QRegExp ("[a-zA-Z0-9-\\@\\_\\#]{5,20}"),this));

    this->styleDefaultInput = ui->inputLogin->styleSheet();
}

LoginWidget::~LoginWidget()
{
    delete ui;
}

QString LoginWidget::getLogin()
{
    return ui->inputLogin->text();
}

QString LoginWidget::getPassword()
{
    return ui->inputPassword->text();
}

void LoginWidget::slotWrongAcc()
{
    ui->inputLogin->clear();
    ui->inputPassword->clear();
    ui->inputLogin->setStyleSheet("background-color: red;");
    ui->inputPassword->setStyleSheet("background-color: red;");
    QTimer::singleShot(1000, this, SLOT(slotWrongAccTimer()));
}

void LoginWidget::slotWrongAccTimer()
{
    this->ui->inputLogin->setStyleSheet(this->styleDefaultInput);
    this->ui->inputPassword->setStyleSheet(this->styleDefaultInput);
}

void LoginWidget::checkNotEmptyForm()
{
    if ((!ui->inputLogin->text().isEmpty()) && (!ui->inputPassword->text().isEmpty()))
    {
        ui->buttonAuth->setEnabled(true);
    }
    else
    {
        ui->buttonAuth->setEnabled(false);
    }
}
