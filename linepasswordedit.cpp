#include "linepasswordedit.h"

LinePasswordEdit::LinePasswordEdit(QWidget *parent)
    : QLineEdit(parent)
{    
    copied = nullptr;
}

void LinePasswordEdit::mousePressEvent(QMouseEvent *event)
{
    if ((event->button() == Qt::MouseButton::LeftButton) && (this->text().length()==12))
    {
        QApplication::clipboard()->setText(this->text());
        if (copied == nullptr)
        copied = new PopUpWidget;
        QRect rect(0.0,0.0,0.0,0.0);
        rect.setX(QApplication::desktop()->screenGeometry().width()-copied->width()-6);
        rect.setY(QApplication::desktop()->screenGeometry().height()-copied->height()-6);
        copied->setGeometry(rect);
        copied->show();
        QTimer::singleShot(1000, copied, SLOT(close()));

    }
    event->accept();
}
