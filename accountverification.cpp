#include "accountverification.h"

AccountVerification::AccountVerification(QObject *parent) :
    QObject(parent)
{

}

void AccountVerification::setLogin(QString login)
{
    this->accLogin = login;//устанавливаем логин
}

void AccountVerification::setPassword(QString password)
{
    QByteArray passwordByte;//создаём массив байтов
    passwordByte.append(password.toUtf8());//помещаем в массив байта наш пароль
    QCryptographicHash hashPassword(QCryptographicHash::Md5);//инициируем объект шифровальщика с шифрованием MD5
    hashPassword.addData(passwordByte);//передаём в объект шифровальщика массив байтов с нашим паролем
    this->accPassword = QString (hashPassword.result().toHex());//сохраняем зашифрованный пароль
}

int AccountVerification::getId()
{
    return this->id;//Возвращаем ID
}


int AccountVerification::getTypeUser()
{
    return this->typeUser;//Возвращаем тип пользования
}

bool AccountVerification::auth()
{
    QSqlQuery authQuery;
    //Начала авторизации, сделал переключатель, чтобы как-то случайно не сломать связку "логин-пароль"
    authQuery.prepare("SELECT id, type FROM workers WHERE login = :login and password = :password;");
    //формируем запрос для авторизации пользователя
    authQuery.bindValue(":login",this->accLogin);//помещаем в запрос логин
    authQuery.bindValue(":password",this->accPassword);//помещаем в запрос пароль
    bool authQueryExec =  authQuery.exec();//делаем запрос и получаем ответ
    if(authQueryExec)
    {//Если запрос удался
        authQuery.next();//берём следующую запись
        if (authQuery.record().value(0) > 0)//если кол-во записей больше нуля, то
        {//Авторизация прошла
            this->id =authQuery.record().value(0).toInt();
            this->typeUser = authQuery.record().value(1).toInt();
            return true;
        }
    }
    else
    {//Если запрос не удался
        emit this->signalDBError();//шлём сигнал об ошибке
    }
    return false;
}
