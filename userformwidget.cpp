#include "userformwidget.h"
#include "ui_userformwidget.h"

UserFormWidget::UserFormWidget(QWidget *parent, int isAdmin) :
    QWidget(parent),
    ui(new Ui::UserFormWidget)
{
    ui->setupUi(this);

    ui->tableOfListOwnersOrObjects->setVisible(false); //Изначально рисуем лого.
    if (isAdmin == 0)
    {
        //Показывать ли админку
        ui->entryToAdmin->setVisible(false);
        ui->labelInfoOfUser->setGeometry(415,20,315,40);
    }

    //Блок связей
    connect(ui->entryToAddOwner,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToAddOwner()));
    connect(ui->entryToAddObject,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToAddObject()));
    connect(ui->entryToEditOwner,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToEditOwner()));
    connect(ui->entryToEditObject,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToEditObject()));
    connect(ui->entryToListOfOwners,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToListOfOwners()));
    connect(ui->entryToListOfObjects,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToListOfObjects()));
    connect(ui->entryToSettings,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToSettings()));
    connect(ui->entryToAdmin,SIGNAL(clicked()),
            this,SIGNAL(signalEntryToAdmin()));

    //cкрытие лого и отображение таблицы
    connect(ui->entryToListOfObjects,SIGNAL(clicked()),
            this,SLOT(slotHideLogo()));
    connect(ui->entryToListOfOwners,SIGNAL(clicked()),
            this,SLOT(slotHideLogo()));  
}

UserFormWidget::~UserFormWidget()
{
    delete ui;
}

void UserFormWidget::updateTableOfListOwnersOrObjects(QSqlQueryModel *model)
{
    ui->tableOfListOwnersOrObjects->setModel(model);
}

void UserFormWidget::changeInfoOfUser(QString *textFioOfUser)
{
    ui->labelInfoOfUser->setText(*textFioOfUser);
}

void UserFormWidget::slotHideLogo()
{
    ui->labelLogo->setVisible(false);
    ui->tableOfListOwnersOrObjects->setVisible(true);
}

