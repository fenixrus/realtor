#ifndef ADDNEWOWNERSWIDGET_H
#define ADDNEWOWNERSWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class AddNewOwnersWidget;
}

class   AddNewOwnersWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AddNewOwnersWidget(QWidget *parent = 0);
    ~AddNewOwnersWidget();
    void getInfoWithForm(QHash<QString, QVariant> *data);

signals:
    void signalAdd();
    void signalSuccess();
private slots:
    void slotReset();
    void slotAdded();
    void on_buttonAdd_clicked();
    void slotSetDefaultStyleForButtonAdd();

private:
    Ui::AddNewOwnersWidget *ui;
    QString styleButtonAdd;
};

#endif // ADDNEWOWNERSWIDGET_H
