#include "settingswidget.h"
#include "ui_settingswidget.h"

SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsWidget)
{
    ui->setupUi(this);

    connect(ui->buttonEdit, SIGNAL(clicked()),
            this,SIGNAL(signalEditSettings()));
    connect(ui->buttonQuery,SIGNAL(clicked()),
            this,SIGNAL(signalQueryOfData()));
    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));
    connect(this,SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));

    ui->firstName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->surName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->fatherName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->login->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z]{1,30}"),this));

    this->styleDefaultEditButton = ui->buttonEdit->styleSheet();
}

SettingsWidget::~SettingsWidget()
{
    delete ui;
}

void SettingsWidget::setInfoIntoForm(QHash<QString, QVariant> *dataForm)
{
    ui->surName->setText(dataForm->value("surName").toString());
    ui->firstName->setText(dataForm->value("firstName").toString());
    ui->fatherName->setText(dataForm->value("fatherName").toString());
    ui->login->setText(dataForm->value("login").toString());
    ui->password->setText(dataForm->value("password").toString());
}

void SettingsWidget::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{
    dataForm->insert("surName", ui->surName->text());
    dataForm->insert("firstName", ui->firstName->text());
    dataForm->insert("fatherName", ui->fatherName->text());
    dataForm->insert("login", ui->login->text());
    if (ui->password->text().length()>6)
        dataForm->insert("password", ui->password->text());
}

void SettingsWidget::slotReset()
{
    ui->surName->clear();
    ui->firstName->clear();
    ui->fatherName->clear();
    ui->login->clear();
    ui->password->clear();
}

void SettingsWidget::slotSetDefaultStyleForEditButton()
{
    ui->buttonEdit->setStyleSheet(this->styleDefaultEditButton);
}

void SettingsWidget::slotAdded()
{
    ui->buttonEdit->setStyleSheet(this->styleDefaultEditButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForEditButton()));
}

void SettingsWidget::slotGenerateNewPassword()
{
    char passChars[] = {
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        '@','#','_'
    };

    srand(time(NULL)+this->countGenerateNewPasswordSymbol + rand());
    int newRand = rand() % 55;
    qDebug() << newRand;
    qDebug() << passChars[newRand];
    ui->password->setText(ui->password->text() + passChars[newRand]);

    if (++this->countGenerateNewPasswordSymbol > 11)
    {
        this->timerGenerateNewPassword->stop();
        ui->buttonEdit->setEnabled(true);
        qDebug() << ui->password->text();
    }
}

void SettingsWidget::on_generateNewPassword_clicked()
{
    ui->buttonEdit->setEnabled(false);
    this->timerGenerateNewPassword = new QTimer;
    this->timerGenerateNewPassword->setInterval(90);
    connect(timerGenerateNewPassword, SIGNAL(timeout()),
            this,SLOT(slotGenerateNewPassword()));
    this->countGenerateNewPasswordSymbol=0;
    ui->password->clear();
    this->timerGenerateNewPassword->start();
}
