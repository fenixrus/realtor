#ifndef EDITOWNERWIDGET_H
#define EDITOWNERWIDGET_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class EditOwnerWidget;
}

class EditOwnerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EditOwnerWidget(QWidget *parent = 0);
    ~EditOwnerWidget();
    void setInfoIntoForm(QHash<QString, QVariant> *dataForm);
    void getInfoWithForm(QHash<QString, QVariant> *dataForm);
    int getTypeWithForm();
    int getIdWithForm();
signals:
    void signalEdit();
    void signalRemove();
    void signalQueryOfData();
    void signalSuccess();
    void signalSuccessRemove();
private slots:
    void slotReset();
    void slotSetDefaultStyleForEditButton();
    void slotSetDefaultStyleForRemoveButton();
    void slotAdded();
    void slotRemoved();

private:
    Ui::EditOwnerWidget *ui;
    QString styleDefaultEditButton;
    QString styleDefaultRemoveButton;
};

#endif // EDITOWNERWIDGET_H
