#include "editownerwidget.h"
#include "ui_editownerwidget.h"

EditOwnerWidget::EditOwnerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditOwnerWidget)
{
    ui->setupUi(this);

    connect(ui->buttonEdit, SIGNAL(clicked()),
            this,SIGNAL(signalEdit()));
    connect(ui->buttonRemove, SIGNAL(clicked()),
            this,SIGNAL(signalRemove()));
    connect(ui->buttonQuery,SIGNAL(clicked()),
            this,SIGNAL(signalQueryOfData()));
    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));
    connect(this,SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));
    connect(this,SIGNAL(signalSuccessRemove()),
            this,SLOT(slotRemoved()));

    ui->firstName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->surName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->fatherName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));

    this->styleDefaultEditButton = ui->buttonEdit->styleSheet();
    this->styleDefaultRemoveButton = ui->buttonRemove->styleSheet();
}

EditOwnerWidget::~EditOwnerWidget()
{
    delete ui;
}

void EditOwnerWidget::setInfoIntoForm(QHash<QString, QVariant> *dataForm)
{
    ui->surName->setText(dataForm->value("surName").toString());
    ui->firstName->setText(dataForm->value("firstName").toString());
    ui->fatherName->setText(dataForm->value("fatherName").toString());
    ui->old->setValue(dataForm->value("old").toInt());
    ui->sex->setCurrentIndex(dataForm->value("sex").toInt());
    ui->phone->setText(dataForm->value("phone").toString());
    ui->desc->setText( dataForm->value("desc").toString());
}

void EditOwnerWidget::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{
    dataForm->insert("id", ui->idOfObject->value());
    dataForm->insert("surName", ui->surName->text());
    dataForm->insert("firstName", ui->firstName->text());
    dataForm->insert("fatherName", ui->fatherName->text());
    dataForm->insert("old", ui->old->value());
    dataForm->insert("sex", ui->sex->currentIndex());
    dataForm->insert("phone", ui->phone->text());
    dataForm->insert("desc", ui->desc->toPlainText());
}

int EditOwnerWidget::getIdWithForm()
{
    return ui->idOfObject->value();
}

void EditOwnerWidget::slotReset()
{
    ui->idOfObject->setValue(0);
    ui->firstName->clear();
    ui->surName->clear();
    ui->fatherName->clear();
    ui->sex->setCurrentIndex(0);
    ui->old->setValue(18);
    ui->phone->clear();
    ui->desc->clear();
}

void EditOwnerWidget::slotSetDefaultStyleForEditButton()
{
    ui->buttonEdit->setStyleSheet(this->styleDefaultEditButton);
}

void EditOwnerWidget::slotSetDefaultStyleForRemoveButton()
{
    ui->buttonRemove->setStyleSheet(this->styleDefaultRemoveButton);
}

void EditOwnerWidget::slotAdded()
{
    ui->buttonEdit->setStyleSheet(this->styleDefaultEditButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForEditButton()));
}

void EditOwnerWidget::slotRemoved()
{
    ui->buttonRemove->setStyleSheet(this->styleDefaultRemoveButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForRemoveButton()));
}
