#include "addnewownerswidget.h"
#include "ui_addnewownerswidget.h"

AddNewOwnersWidget::AddNewOwnersWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddNewOwnersWidget)
{
    ui->setupUi(this);
    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));
    connect(this,SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));

    this->styleButtonAdd = ui->buttonAdd->styleSheet();
    ui->firstName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->surName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->fatherName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
}

AddNewOwnersWidget::~AddNewOwnersWidget()
{
    delete ui;
}

void AddNewOwnersWidget::getInfoWithForm(QHash<QString, QVariant> *data)
{
    data->insert("surName", ui->surName->text());
    data->insert("firstName", ui->firstName->text());
    data->insert("fatherName", ui->fatherName->text());
    data->insert("phone", ui->phone->text());
    data->insert("sex", ui->sex->currentIndex());
    data->insert("old", ui->old->value());
    data->insert("desc", ui->desc->toPlainText());
}
void AddNewOwnersWidget::slotReset()
{
    ui->firstName->clear();
    ui->surName->clear();
    ui->fatherName->clear();
    ui->desc->clear();
    ui->sex->setCurrentIndex(0);
    ui->old->setValue(18);
    ui->phone->clear();
}

void AddNewOwnersWidget::slotAdded()
{
    ui->buttonAdd->setStyleSheet(this->styleButtonAdd + "background: lightgreen;");
    QTimer::singleShot(500, this, SLOT(slotSetDefaultStyleForButtonAdd()));
}

void AddNewOwnersWidget::on_buttonAdd_clicked()
{
    if ((ui->surName->text().length() > 1)
            and (ui->firstName->text().length() > 1)
            and (ui->fatherName->text().length( ) > 1)
            and (ui->phone->text().length() == 17))
        emit this->signalAdd();
    else
        QMessageBox::warning(this,"Пустые поля", "Заполните, пожалуйста все поля.");
}

void AddNewOwnersWidget::slotSetDefaultStyleForButtonAdd()
{
    ui->buttonAdd->setStyleSheet(this->styleButtonAdd);
}
