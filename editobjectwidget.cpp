#include "editobjectwidget.h"
#include "ui_editobjectwidget.h"

EditObjectWidget::EditObjectWidget(QWidget *parent,
                                   QHash<int, QString> *cities,
                                   QHash<int, QString> *areas,
                                   QHash<int, QString> *typeOfObject) :
    QWidget(parent),
    ui(new Ui::EditObjectWidget)
{
    ui->setupUi(this);
    this->styleDefaultEditButton = ui->buttonEdit->styleSheet();
    this->styleDefaultRemoveButton = ui->buttonRemove->styleSheet();

    ui->groupBoxForm->setEnabled(false);
    ui->groupOfRent->setEnabled(false);

    ui->street->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));

    connect(ui->buttonEdit, SIGNAL(clicked()),
            this,SIGNAL(signalEdit()));
    connect(ui->buttonRemove, SIGNAL(clicked()),
            this,SIGNAL(signalRemove()));
    connect(ui->buttonQuery,SIGNAL(clicked()),
            this,SIGNAL(signalQueryOfData()));
    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));
    connect(this,SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));
    connect(this,SIGNAL(signalSuccessRemove()),
            this,SLOT(slotRemoved()));

    this->selectsFill(cities,ui->selectcity);
    this->selectsFill(areas,ui->selectarea);
    this->selectsFill(typeOfObject,ui->selectTypeOfObject);

}
EditObjectWidget::~EditObjectWidget()
{
    delete ui;
}

void EditObjectWidget::setInfoIntoForm(QHash<QString, QVariant> *dataForm)
{
    ui->selectTypeOfObject->setCurrentIndex(ui->selectTypeOfObject->findData(dataForm->value("typeOfObject").toInt()));
    ui->numberHouse->setValue(dataForm->value("numberHouse").toInt());
    ui->numberApartment->setValue(dataForm->value("numberApartment").toInt());
    ui->street->setText(dataForm->value("street").toString());
    ui->square->setValue(dataForm->value("square").toInt());
    ui->needInComfort->setChecked(dataForm->value("needInComfort").toBool());
    ui->needInBalcony->setChecked(dataForm->value("needInBalcony").toBool());
    ui->needInLoggia->setChecked(dataForm->value("needInLoggia").toBool());
    ui->countOfStoreys->setValue(dataForm->value("countOfStoreys").toInt());
    ui->countOfRooms->setValue(dataForm->value("countOfRooms").toInt());
    ui->idOfOwner->setValue(dataForm->value("idOfOwner").toInt());
    ui->selectcity->setCurrentIndex(dataForm->value("city").toInt());
    ui->selectarea->setCurrentIndex(dataForm->value("area").toInt());
    ui->desc->setPlainText(dataForm->value("desc").toString());

    if (ui->searchSaleOrRent->currentIndex() == 1)
    {
        ui->priceOfSale->setValue(dataForm->value("priceOfSale").toInt());
    }
    if (ui->searchSaleOrRent->currentIndex() == 2)
    {
        ui->countOfRoomsUnderRent->setValue(dataForm->value("countOfRoomsUnderRent").toInt());
        ui->accommodationWithAOwner->setChecked(dataForm->value("accommodationWithAOwner").toBool());
        ui->priceOfRent->setValue(dataForm->value("priceOfRent").toInt());
        ui->timeOfRent->setCurrentIndex(dataForm->value("timeOfRent").toInt());
    }
}

void EditObjectWidget::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{

    /*QHash<QString, QString> dataForm;
     * [searchSaleOrRent] = Sale/Rent
     * [selectTypeOfObject] = комната=0, квартира=1, дом с участком=2, дом без участка=3, участок=4
     *[area] = 0... (выборка из таблицы area)
     *[city] = 0... (выборка из таблицы city)
     *[needInComfort] = 0/1
     *[needInBalcony] = 0/1
     *[needInLoggia] = 0/1
     *[countOfStoreys] = 1...
     *[countOfRooms] = 1...
     *[fioOfOwner] = 1...
     *[street] = abc...
     *[numberHouse] = 1...
     *[numberApartment] = 1...
     *[square] = 1...
     *[priceOfSale] = 1...
     **[desc] = abc....
     *For rent
     **[timeOfRent] = 0 любое, 1... (выборка из таблицы time_of_rent)
     **[accommodationWithAOwner] = 1/0
     **[countRoomsOfRent] = 1...
     **[priceOfRent] = 1...
     */

    dataForm->insert("idOfObject",  ui->idOfObject->value());
    dataForm->insert("searchSaleOrRent",  ui->searchSaleOrRent->currentIndex());
    dataForm->insert("typeOfObject", ui->selectTypeOfObject->currentIndex());
    dataForm->insert("area",  ui->selectarea->currentIndex());
    dataForm->insert("city",   ui->selectcity->currentIndex());
    dataForm->insert("needInComfort", ui->needInComfort->isChecked());
    dataForm->insert("needInBalcony",  ui->needInBalcony->isChecked());
    dataForm->insert("needInLoggia",  ui->needInLoggia->isChecked());
    dataForm->insert("numberApartment", ui->numberApartment->value());
    dataForm->insert("countOfStoreys", ui->countOfStoreys->value());
    dataForm->insert("countOfRooms",  ui->countOfStoreys->value());
    dataForm->insert("numberHouse", ui->numberHouse->value());
    dataForm->insert("square", ui->square->value());
    dataForm->insert("street", ui->street->text());
    dataForm->insert("owner", ui->idOfOwner->value());
    dataForm->insert("desc", ui->desc->toPlainText());

    if (ui->searchSaleOrRent->currentIndex() == 1) //Если продажа
    {
        dataForm->insert("priceOfSale", ui->priceOfSale->value());
    }
    if (ui->searchSaleOrRent->currentIndex() == 2)
    {
        dataForm->insert("timeOfRent",   ui->timeOfRent->currentIndex());
        dataForm->insert("needInBalcony",  ui->accommodationWithAOwner->isChecked());
        dataForm->insert("countOfRoomsUnderRent", ui->countOfRoomsUnderRent->value());
        dataForm->insert("priceOfRent", ui->priceOfRent->value());
    }
}

int EditObjectWidget::getIdWithForm()
{
    return ui->idOfObject->value();
}

int EditObjectWidget::getTypeTicketWithForm()
{
    return ui->searchSaleOrRent->currentIndex();
}

void EditObjectWidget::slotReset()
{
    ui->accommodationWithAOwner->setChecked(false);
    ui->countOfRooms->setValue(1);
    ui->countOfStoreys->setValue(1);
    ui->countOfRoomsUnderRent->setValue(1);
    ui->needInBalcony->setChecked(false);
    ui->needInComfort->setChecked(false);
    ui->needInLoggia->setChecked(false);
    ui->searchSaleOrRent->setCurrentIndex(0);
    ui->selectarea->setCurrentIndex(0);
    ui->selectcity->setCurrentIndex(0);
    ui->selectTypeOfObject->setCurrentIndex(0);
    ui->groupOfRent->setEnabled(false);
    ui->desc->clear();
    ui->numberHouse->setValue(1);
    ui->numberApartment->setValue(1);
    ui->priceOfRent->setValue(1);
    ui->priceOfSale->setValue(1);
    ui->street->clear();
    ui->idOfObject->setValue(0);
    ui->idOfOwner->setValue(0);
}

void EditObjectWidget::slotSetDefaultStyleForEditButton()
{
    ui->buttonEdit->setStyleSheet(this->styleDefaultEditButton);
}

void EditObjectWidget::slotSetDefaultStyleForRemoveButton()
{
    ui->buttonRemove->setStyleSheet(this->styleDefaultRemoveButton);
}

void EditObjectWidget::slotAdded()
{
    ui->buttonEdit->setStyleSheet(this->styleDefaultEditButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForEditButton()));
}

void EditObjectWidget::slotRemoved()
{
    ui->buttonRemove->setStyleSheet(this->styleDefaultRemoveButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForRemoveButton()));
}

//You are expelled;

void EditObjectWidget::on_searchSaleOrRent_currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->groupBoxForm->setEnabled(false);
        ui->groupOfRent->setEnabled(false);
        break;
    case 1:
        ui->groupBoxForm->setEnabled(true);
        ui->groupOfRent->setEnabled(false);
        ui->priceOfSale->setEnabled(true);
        break;
    case 2:
        ui->groupBoxForm->setEnabled(true);
        ui->groupOfRent->setEnabled(true);
        ui->priceOfSale->setEnabled(false);
        break;
    }
}

void EditObjectWidget::selectsFill(QHash<int, QString> *data, QComboBox *select)
{
    if (data != nullptr)
    {
        QHash<int, QString>::iterator i;
        int index = 1;
        for (i = data->begin(); i != data->end(); ++i, index++)
        {
            select->insertItem(index, i.value(), i.key());
        }
    }
}
