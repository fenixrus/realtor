#ifndef USERFORMWIDGET_H
#define USERFORMWIDGET_H

#include <QWidget>
#include <QSqlQueryModel>

namespace Ui {
class UserFormWidget;
}

class UserFormWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UserFormWidget(QWidget *parent = 0, int isAdmin=0);
    ~UserFormWidget();
    void updateTableOfListOwnersOrObjects(QSqlQueryModel *model);
    void changeInfoOfUser(QString *textFioOfUser);

public slots:

private slots:
    void slotHideLogo();

signals:
    void signalEntryToAddOwner();
    void signalEntryToAddObject();
    void signalEntryToEditOwner();
    void signalEntryToEditObject();
    void signalStepToNextPage();
    void signalStepToLastPage();
    void signalEntryToListOfOwners();
    void signalEntryToListOfObjects();
    void signalEntryToSettings();
    void signalEntryToAdmin();
private:
    Ui::UserFormWidget *ui;
};

#endif // USERFORMWIDGET_H
