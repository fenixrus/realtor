#-------------------------------------------------
#
# Project created by QtCreator 2016-03-22T11:22:12
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = realtor
TEMPLATE = app
CONFIG += c++11

SOURCES += \
    accountverification.cpp \
    addnewobjectwidget.cpp \
    addnewownerswidget.cpp \
    controlviewwidget.cpp \
    generalwidget.cpp \
    listofobjects.cpp \
    listofowners.cpp \
    loginwidget.cpp \
    main.cpp \
    userformwidget.cpp \
    editownerwidget.cpp \
    editobjectwidget.cpp \
    settingswidget.cpp \
    adminpanelwidget.cpp \
    linepasswordedit.cpp \
    popupwidget.cpp

HEADERS  += \
    accountverification.h \
    addnewobjectwidget.h \
    addnewownerswidget.h \
    controlviewwidget.h \
    generalwidget.h \
    listofobjects.h \
    listofowners.h \
    loginwidget.h \
    userformwidget.h \
    editownerwidget.h \
    editobjectwidget.h \
    settingswidget.h \
    adminpanelwidget.h \
    linepasswordedit.h \
    popupwidget.h

FORMS    += \
    addnewobjectwidget.ui \
    addnewownerswidget.ui \
    generalwidget.ui \
    listofobjects.ui \
    listofowners.ui \
    loginwidget.ui \
    userformwidget.ui \
    editownerwidget.ui \
    editobjectwidget.ui \
    settingswidget.ui \
    adminpanelwidget.ui \
    popupwidget.ui

RESOURCES += \
    img.qrc

