#ifndef ACCOUNTVERIFICATION_H
#define ACCOUNTVERIFICATION_H

#include <QObject>//Наследуемся от него, чтобы использовать все возможности qt`а
#include <QtSql>//Использование sql в классе
#include <QByteArray>//Класс массива байтов
class AccountVerification : public QObject
{
    Q_OBJECT
public:
    explicit AccountVerification(QObject *parent = 0);//конструктор
    void setLogin(QString login);//установка логина
    void setPassword(QString password);//установка пароля
    int getId();//получить ID залогиненного пользователя
    int getTypeUser();//получить тип юзера
    bool auth();//метод для запуска авторизации
signals:
    void signalDBError();//сигнал ошибки БД

private:
    QString accLogin;//Строка с логином
    QString accPassword;//Строка с паролем
    int id;//ID
    int typeUser;//Тип пользователя

};

#endif // ACCOUNTVERIFICATION_H
