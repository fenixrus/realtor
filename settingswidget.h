#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QDebug>
#include <stdlib.h>
#include <ctime>

namespace Ui {
class SettingsWidget;
}

class SettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsWidget(QWidget *parent = 0);
    ~SettingsWidget();
    void setInfoIntoForm(QHash<QString, QVariant> *dataForm);
    void getInfoWithForm(QHash<QString, QVariant> *dataForm);
signals:
    void signalEditSettings();
    void signalQueryOfData();
    void signalSuccess();
private slots:
    void slotReset();
    void slotSetDefaultStyleForEditButton();
    void slotAdded();
    void slotGenerateNewPassword();

    void on_generateNewPassword_clicked();

private:
    Ui::SettingsWidget *ui;
    QString styleDefaultEditButton;
    QTimer *timerGenerateNewPassword;
    int countGenerateNewPasswordSymbol;
};

#endif // SETTINGSWIDGET_H
