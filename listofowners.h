#ifndef LISTOFOWNERS_H
#define LISTOFOWNERS_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class ListOfOwners;
}

class ListOfOwners : public QWidget
{
    Q_OBJECT

public:
    explicit ListOfOwners(QWidget *parent = 0);
    ~ListOfOwners();
    //
    void getInfoWithForm(QHash<QString, QVariant> *dataForm);
    //
signals:
    void signalSearchOwner();
    void signalSuccess();
    //
private slots:
    void slotReset();
    void slotSetDefaultStyleForAddButton();
    void slotAdded();
    //
private:
    Ui::ListOfOwners *ui;
    QString styleDefaultAddButton;
};

#endif // LISTOFOWNERS_H
