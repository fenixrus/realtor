#include "listofobjects.h"
#include "ui_listofobjects.h"

ListOfObjects::ListOfObjects(QWidget *parent,
                             QHash<int, QString> *cities,
                             QHash<int, QString> *areas,
                             QHash<int, QString> *typeOfObject) :
    QWidget(parent),
    ui(new Ui::ListOfObjects)
{
    ui->setupUi(this);

    ui->groupOfRent->setEnabled(false);

    this->selectsFill(cities,ui->selectcity);
    this->selectsFill(areas,ui->selectarea);
    this->selectsFill(typeOfObject,ui->selectTypeOfObject);

    //соединение кнопки "Поиск" с сигналом начало поиска(будет ловиться в controlviewwidget
    connect(ui->pushButtonSearch, SIGNAL(clicked()),
            this,SIGNAL(signalSearchObject()));
    connect(ui->pushButtonReset,SIGNAL(clicked(bool)),
            this, SLOT(slotReset()));
    connect(this, SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));
    this->styleDefaultEditButton = ui->pushButtonSearch->styleSheet();
}

ListOfObjects::~ListOfObjects()
{
    delete ui;
}

void ListOfObjects::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{

    /*QHash<QString, QString> dataForm;
     * [searchSaleOrRent] = Sale/Rent
     * [typeOfObject] = комната=0, квартира=1, дом с участком=2, дом без участка=3, участок=4
     *[area] = 0... (выборка из таблицы area)
     *[city] = 0... (выборка из таблицы city)
     *[needInComfort] = 0/1
     *[needInBalcony] = 0/1
     *[needInLoggia] = 0/1
     *[countOfStoreys] = 1...
     *[countOfRooms = 1...
     *For rent
     **[timeOfRent] = 0 любое, 1... (выборка из таблицы time_of_rent)
     **[accommodationWithAOwner] = 1/0
     **[countRoomsOfRent] = 1...
     */
    dataForm->insert("listMyRecord", ui->listMyRecord->isChecked());
    dataForm->insert("searchSaleOrRent",  ui->searchSaleOrRent->currentIndex());
    dataForm->insert("typeOfObject", ui->selectTypeOfObject->currentIndex());
    dataForm->insert("area",  ui->selectarea->currentIndex());
    dataForm->insert("city",   ui->selectcity->currentIndex());
    dataForm->insert("needInComfort", static_cast<int>(ui->needInComfort->isChecked()));
    dataForm->insert("needInBalcony",  static_cast<int>(ui->needInBalcony->isChecked()));
    dataForm->insert("needInLoggia",  static_cast<int>(ui->needInLoggia->isChecked()));
    dataForm->insert("countOfStoreys", ui->countOfStoreys->value());
    dataForm->insert("countOfRooms",  ui->countOfRooms->value());
    if (ui->searchSaleOrRent->currentIndex() == 1) //Если съём
    {
        dataForm->insert("city",   ui->timeOfRent->currentIndex());
        dataForm->insert("needInBalcony",  ui->accommodationWithAOwner->isChecked());
        dataForm->insert("countOfStoreys", ui->countRoomsOfRent->value());
    }
}

void ListOfObjects::slotAdded()
{
    ui->pushButtonSearch->setStyleSheet(this->styleDefaultEditButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForEditButton()));
}

void ListOfObjects::on_searchSaleOrRent_currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->groupOfRent->setEnabled(false);
        break;
    case 1:
        ui->groupOfRent->setEnabled(true);
        break;
    }
}

void ListOfObjects::slotReset()
{
    ui->listMyRecord->setChecked(false);
    ui->accommodationWithAOwner->setChecked(false);
    ui->countOfRooms->setValue(0);
    ui->countOfStoreys->setValue(0);
    ui->countRoomsOfRent->setValue(0);
    ui->needInBalcony->setChecked(false);
    ui->needInComfort->setChecked(false);
    ui->needInLoggia->setChecked(false);
    ui->searchSaleOrRent->setCurrentIndex(0);
    ui->selectarea->setCurrentIndex(0);
    ui->selectcity->setCurrentIndex(0);
    ui->selectTypeOfObject->setCurrentIndex(0);
    ui->groupOfRent->setEnabled(false);
}

void ListOfObjects::slotSetDefaultStyleForEditButton()
{
    ui->pushButtonSearch->setStyleSheet(this->styleDefaultEditButton);
}

void ListOfObjects::selectsFill(QHash<int, QString> *data, QComboBox *select)
{
    if (data != nullptr)
    {
        QHash<int, QString>::iterator i;
        int index = 1;
        for (i = data->begin(); i != data->end(); ++i, index++)
        {
            select->insertItem(index, i.value(), QVariant(i.key()));
        }
    }
}
