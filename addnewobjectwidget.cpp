#include "addnewobjectwidget.h"
#include "ui_addnewobjectwidget.h"

//Класс виджета добавления нового объекта
AddNewObjectWidget::AddNewObjectWidget(QWidget *parent,
                                       QHash <int, QString> *fio, //список владельцев
                                       QHash <int, QString> *cities,//список городов
                                       QHash <int, QString> *areas,//список районов
                                       QHash <int, QString> *typeOfObject) ://список типов объекта
    QWidget(parent),
    ui(new Ui::AddNewObjectWidget)
{
    ui->setupUi(this);

    this->selectsFill(fio,ui->fioOfOwner);//Заполнение нужного комбобокса списоком
    this->selectsFill(cities,ui->selectcity);//Заполнение нужного комбобокса списоком
    this->selectsFill(areas,ui->selectarea);//Заполнение нужного комбобокса списоком
    this->selectsFill(typeOfObject,ui->selectTypeOfObject);//Заполнение нужного комбобокса списоком

    ui->fioOfOwner->setCurrentIndex(-1);
    ui->fioOfOwner->setCurrentText("Выбор владельца"); //Сброс комбобокса в "нулевое" состояние

    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));
    connect(this,SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));
    connect(ui->buttonAdd,SIGNAL(clicked()),
            this,SIGNAL(signalAdd()));
    this->ui->groupOfRent->setEnabled(false);
    ui->fioOfOwner->setCurrentText("Выбор владельца");

    ui->street->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    this->styleDefaultEditButton = ui->buttonAdd->styleSheet();
}

AddNewObjectWidget::~AddNewObjectWidget()
{
    delete ui;
}

void AddNewObjectWidget::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{

    /*QHash<QString, QString> dataForm;
     * [searchSaleOrRent] = Sale/Rent
     * [typeOfObject] = комната=0, квартира=1, дом с участком=2, дом без участка=3, участок=4
     *[area] = 0... (выборка из таблицы area)
     *[city] = 0... (выборка из таблицы city)
     *[needInComfort] = 0/1
     *[needInBalcony] = 0/1
     *[needInLoggia] = 0/1
     *[countOfStoreys] = 1...
     *[countOfRooms] = 1...
     *[idOfOwner] = 1...
     *[street] = abc...
     *[numberHouse] = 1...
     *[numberApartment] = 1...
     *[square] = 1...
     *[priceOfSale] = 1...
     **[desc] = abc....
     *For rent
     **[timeOfRent] = 0 любое, 1... (выборка из таблицы time_of_rent)
     **[accommodationWithAOwner] = 1/0
     **[countOfRoomsUnderRent] = 1...
     **[priceOfRent] = 1...
     */

    dataForm->insert("searchSaleOrRent",  ui->searchSaleOrRent->currentIndex());
    dataForm->insert("typeOfObject", ui->selectTypeOfObject->currentData());
    dataForm->insert("area",  ui->selectarea->currentData());
    dataForm->insert("city",   ui->selectcity->currentData());
    dataForm->insert("needInComfort", ui->needInComfort->isChecked());
    dataForm->insert("needInBalcony",  ui->needInBalcony->isChecked());
    dataForm->insert("needInLoggia",  ui->needInLoggia->isChecked());
    dataForm->insert("numberApartment", ui->numberApartment->value());
    dataForm->insert("countOfStoreys", ui->countOfStoreys->value());
    dataForm->insert("countOfRooms",  ui->countOfStoreys->value());
    dataForm->insert("numberHouse", ui->numberHouse->value());
    dataForm->insert("square", ui->square->value());
    dataForm->insert("street", ui->street->text());
    dataForm->insert("desc", ui->desc->toPlainText());
    dataForm->insert("idOfOwner", ui->fioOfOwner->currentData());

    if (ui->searchSaleOrRent->currentIndex() == 0) //Если продажа
        dataForm->insert("priceOfSale", ui->priceOfSale->value());
    else
    {
        dataForm->insert("timeOfRent",   ui->timeOfRent->currentIndex());
        dataForm->insert("needInBalcony",  ui->accommodationWithAOwner->isChecked());
        dataForm->insert("countOfStoreys", ui->countOfRoomsUnderRent->value());
        dataForm->insert("priceOfRent", ui->priceOfRent->value());
    }
}

void AddNewObjectWidget::setComboBoxFio(QHash<int, QString> *fio)
{
    if (fio != nullptr)
    {
        ui->fioOfOwner->clear();
        ui->idOfOwner->setValue(1);
        this->selectsFill(fio,ui->fioOfOwner);
    }
}

void AddNewObjectWidget::slotReset()
{
    ui->accommodationWithAOwner->setChecked(false);
    ui->countOfRooms->setValue(1);
    ui->countOfStoreys->setValue(1);
    ui->countOfRoomsUnderRent->setValue(1);
    ui->needInBalcony->setChecked(false);
    ui->needInComfort->setChecked(false);
    ui->needInLoggia->setChecked(false);
    ui->searchSaleOrRent->setCurrentIndex(0);
    ui->selectarea->setCurrentIndex(0);
    ui->selectcity->setCurrentIndex(0);
    ui->selectTypeOfObject->setCurrentIndex(0);
    ui->groupOfRent->setEnabled(false);
    ui->desc->clear();
    ui->numberHouse->setValue(1);
    ui->numberApartment->setValue(1);
    ui->priceOfRent->setValue(1);
    ui->priceOfSale->setValue(1);
    ui->fioOfOwner->setCurrentIndex(-1);
    ui->fioOfOwner->setCurrentText("Выбор владельца");
    ui->street->clear();
}

void AddNewObjectWidget::slotSetDefaultStyleForButtonAdd()
{
    ui->buttonAdd->setStyleSheet(this->styleDefaultEditButton);
}

void AddNewObjectWidget::slotAdded()
{
    ui->buttonAdd->setStyleSheet(this->styleDefaultEditButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForButtonAdd()));
}

void AddNewObjectWidget::selectsFill(QHash<int, QString> *data, QComboBox *select)
{
    if (data != nullptr)
    {
        QHash<int, QString>::iterator i;
        int index = 0;
        for (i = data->begin(); i != data->end(); ++i, index++)
        {
            select->insertItem(index, i.value(), QVariant(i.key()));
        }
    }
}

void AddNewObjectWidget::on_searchSaleOrRent_currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->groupOfRent->setEnabled(false);
        ui->priceOfSale->setEnabled(true);
        break;
    case 1:
        ui->groupOfRent->setEnabled(true);
        ui->priceOfSale->setEnabled(false);
        break;
    }
}
void AddNewObjectWidget::on_idOfOwner_valueChanged(int id)
{
    int dataId = ui->fioOfOwner->findData(QVariant(id));
    ui->fioOfOwner->setCurrentIndex(dataId != -1 ? dataId : 1);
}
void AddNewObjectWidget::on_fioOfOwner_currentIndexChanged()
{
    ui->idOfOwner->setValue(ui->fioOfOwner->currentData().toInt());
}
