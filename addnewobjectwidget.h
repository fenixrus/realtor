#ifndef ADDNEWOBJECTWIDGET_H
#define ADDNEWOBJECTWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QTimer>

namespace Ui {
class AddNewObjectWidget;
}

class AddNewObjectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AddNewObjectWidget(QWidget *parent = 0,
                                QHash <int, QString> *fio = nullptr,
                                QHash <int, QString> *cities = nullptr,
                                QHash <int, QString> *areas = nullptr,
                                QHash <int, QString> *typeOfObject = nullptr);
    ~AddNewObjectWidget();
    void getInfoWithForm(QHash<QString, QVariant> *dataForm = nullptr);
    void setComboBoxFio(QHash <int, QString> *fio = nullptr);
signals:
    void signalAdd();
    void signalSuccess();
private slots:
    void slotReset();
    void slotSetDefaultStyleForButtonAdd();
    void slotAdded();
    void on_searchSaleOrRent_currentIndexChanged(int index);
    void on_idOfOwner_valueChanged(int id);
    void on_fioOfOwner_currentIndexChanged();

private:
    void selectsFill(QHash<int, QString> *data, QComboBox *select);
    Ui::AddNewObjectWidget *ui;
    QString styleDefaultEditButton;
};

#endif // ADDNEWOBJECTWIDGET_H
