#ifndef CONTROLVIEWWIDGET_H
#define CONTROLVIEWWIDGET_H

#include <QObject>//Главный класс для Qt от которого наследуются многие другие, включая этот.
#include <QtSql>//Подключение класса Sql
#include <QDebug>//Подключения класса для писание отладочной информации в консоль
#include <QMessageBox>//Подключение класса всплывающих окошек
#include <QDesktopWidget>

#include "generalwidget.h"//Окно-виджет
#include "accountverification.h"//Класс авторизации
#include "loginwidget.h"//Класс виджета формы авторизации
#include "userformwidget.h"//Класс виджета главного меню
#include "listofowners.h"//Класс виджета поиска владельцев
#include "addnewownerswidget.h"//Класс виджета добавление владельца
#include "editownerwidget.h"//Класс виджета редактирование владельца
#include "listofobjects.h"//Класс виджета поиска объектов
#include "addnewobjectwidget.h"//Класс виджета добавления объекта
#include "editobjectwidget.h"//Класс виджета редактирование объекта
#include "settingswidget.h"//Класс виджета пользовательских настроек
#include "adminpanelwidget.h"//Класс виджета админ-панели

class ControlViewWidget : public QObject
{
    Q_OBJECT //Позволяет использовать Qt MOC в своём проекте
public://Начало объявления публичных полей/методов
    explicit ControlViewWidget(QObject *parent = 0);//конструктор главного класса с указанием родителя(по умолчанию 0 = нет родителя)
    ~ControlViewWidget();//деструктор главного класса
    //
 private slots://
    void slotEntryToListOfOwners();//Обработчик нажатия кнопки поиска владельцев главного меню
    void slotEntryToListOfObjects();//Обработчик нажатия кнопки поиска объектов главного меню
    void slotEntryToEditOwner();//Обработчик нажатия кнопки редактирование владельца главного меню
    void slotEntryToEditObject();//Обработчик нажатия кнопки редактирование объекта главного меню
    void slotEntryToAddOwner();//Обработчик нажатия кнопки добавление владельца главного меню
    void slotEntryToAddObject();//Обработчик нажатия кнопки добавление объекта главного меню
    void slotEntryToSettings();//Обработчик нажатия кнопки настроек главного меню
    void slotEntryToAdmin();//Обработчик нажатия кнопки админ-панели главного меню
    //
    void slotLoginFormAccepted();//метод вызываемый при удачной авторизации
    void slotAddObject();//метод вызываемый при добавлении объекта
    void slotEditObject();//метод вызываемый при редактировании объекта
    void slotRemoveObject();//метод вызываемый при удалении объекта
    void slotSearchObject();//метод вызываемый при поиске объекта
    void slotQueryOfDataObject();//метод вызываемый при получении данных объекта
    void slotAddOwner();//метод вызываемый при добавлении владельца
    void slotEditOwner();//метод вызываемый при изменении владельца
    void slotRemoveOwner();//метод вызываемый при удалении владельца
    void slotSearchOwner();//метод вызываемый при поиска владельца
    void slotQueryOfDataOwner();//метод вызываемый при получении данных владельца
    void slotEditSettings();//метод вызываемый при редактировании настроек
    void slotQueryOfDataSettings();//метод вызываемый при получении текущих настроек
    void slotAddOrEditWorker();//метод вызываемый при добавлении или изменении работника
    void slotQueryOfDataWorker();//метод вызываемый при получении данных работника
    void slotRemoveWorker();//метод вызываемый при удалении работника
    void slotShowWorkers(int index);//метод вызываемый при отображениии работников
    //
    void slotCloseApplication();//метод вызываемый при закрытии приложении
    void DBError(QWidget *parentMessage = nullptr, QString mess = NULL);// служебный метод для отображении всплывающнего окошка об ошибке
    //
private://начало приватных поле/методов
    QSqlDatabase dbase;//Контекст БД
    GeneralWidget *baseWidget; //главный виджет(окно)
    //
    //Дополнительные виджеты
    LoginWidget *loginForm;//Виджет формы авторизации
    UserFormWidget *userWidget;//Виджет формы Главного меню
    ListOfOwners *listOfOwnersWidget;//Виджет формы Поиск владельцев
    AddNewOwnersWidget *addNewOwner;//Виджет формы добавления
    EditOwnerWidget *editOwner;//Виджет формы редактирование владельцев
    ListOfObjects *listOfObjectsWidget;//Виджет формы список объектов
    AddNewObjectWidget *addNewObject;//Виджет формы добавление нового объекта
    EditObjectWidget *editObject;//Виджет формы редактирование объекта
    SettingsWidget *userSettings;//Виджет формы настройки пользователя
    AdminPanelWidget *adminPanel;//Виджет формы админ-панель
    //
    QWidget *lastWidget;//Последний установленный виджет
    //
    //Информация о текущем пользователе
    int id;//Его ID в БД
    int typeUser;//Тип(Юзер = 0, Админ = 1)
    //
    QHash <int, QString> *cities;//список городов
    QHash <int, QString> *areas;//список областей
    QHash <int, QString> *typeOfObject;//Варианты объекта недвижимости
    //
    void showWidget(QWidget *changeWidget);//Отображение нового виджета
    void changeNameUser();//Смена имени пользователя в главном меню при обновлении данных в БД
    void insertWhereInSqlQuery(QString *str, QString key, QVariant value);
    //служебный метод, нужен для формировании запроса sql
    void insertWhereInSqlQuery(QString *str, QString key, QString value);
    //служебный метод, нужен для формировании запроса sql. Перегруженный метод
    //
};

#endif // CONTROLVIEWWIDGET_H
