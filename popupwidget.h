#ifndef POPUPWIDGET_H
#define POPUPWIDGET_H

#include <QWidget>

namespace Ui {
class PopUpWidget;
}

class PopUpWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PopUpWidget(QWidget *parent = 0);
    ~PopUpWidget();

private:
    Ui::PopUpWidget *ui;
};

#endif // POPUPWIDGET_H
