#ifndef LISTOFOBJECTS_H
#define LISTOFOBJECTS_H

#include <QWidget>
#include <QComboBox>
#include <QTimer>

namespace Ui {
class ListOfObjects;
}

class ListOfObjects : public QWidget
{
    Q_OBJECT

public:
    explicit ListOfObjects(QWidget *parent = 0,
                           QHash <int, QString> *cities = nullptr,
                           QHash <int, QString> *areas = nullptr,
                           QHash <int, QString> *typeOfObject = nullptr);
    ~ListOfObjects();

    void getInfoWithForm(QHash<QString, QVariant> *dataForm);

signals:
    void signalSearchObject();
    void signalSuccess();
private slots:
    void slotAdded();
    void slotReset();
    void slotSetDefaultStyleForEditButton();
    void on_searchSaleOrRent_currentIndexChanged(int index);
private:
    void selectsFill(QHash<int, QString> *data, QComboBox *select);
    Ui::ListOfObjects *ui;
    QString styleDefaultEditButton;
};

#endif // LISTOFOBJECTS_H
