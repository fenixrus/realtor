#include "controlviewwidget.h"//Подключение заголовочного файла

ControlViewWidget::ControlViewWidget(QObject *parent) : QObject(parent)
{
    this->lastWidget = nullptr;//Обнуление указателя на последний виджет
    this->loginForm = nullptr;//Обнуление указателя на виджет авторизации
    this->userWidget = nullptr;//Обнуление указателя на виджет главного окна
    this->addNewOwner = nullptr;//Обнуление указателя на виджет добавление владельца
    this->listOfOwnersWidget = nullptr;//Обнуление указателя на виджет поиска владельцев
    this->editOwner = nullptr;//Обнуление указателя на виджет редактирование владельца
    this->addNewObject = nullptr;//Обнуление указателя на виджет добавление объекта
    this->listOfObjectsWidget = nullptr;//Обнуление указателя на виджет поиска объекта
    this->editObject = nullptr;//Обнуление указателя на виджетр редактирование объекта
    this->userSettings = nullptr;//Обнуление указателя на виджет настроек пользователя
    this->adminPanel = nullptr;//Обнуление указателя на виджет админ-панели

    this->baseWidget = new GeneralWidget;//Инициализация базового виджета
    connect(this->baseWidget, SIGNAL(closeApplication()),
            this, SLOT(slotCloseApplication()));
    //соединение слота закрытия базового виджета со слотом закрытия данного класса
    this->baseWidget->show();//
    /*
     * Нужен чтобы при закрытии приложения его второстепенные виджета закрывались
     * автоматически вместе с главным окном
     */

    dbase = QSqlDatabase::addDatabase("QSQLITE");//Задаём тип используемой БД
    dbase.setDatabaseName("D:/qt_projects/realtor.sqlite");//Задаём файл для БД
    dbase.setDatabaseName("./realtor.sqlite");//Задаём файл для БД
    if (!this->dbase.open())//Если БД не открыта
        QMessageBox::warning(
                    this->baseWidget,
                    "Ошибка доступа к БД",
                    "Нет соединения с БД." + dbase.lastError().text());
    //Выдаём предупреждение всплывающем окном
    QSqlQuery *cityAndAreaAndType = new QSqlQuery;//Инициализируем и выделяем память под объект запросов sql
    this->cities = new QHash <int, QString>;
    //Инициализируем и выделяем память под объект хеш-таблица городов
    this->areas = new QHash <int, QString>;
    //Инициализируем и выделяем память под объект хеш-таблица районов
    this->typeOfObject = new QHash <int, QString>;
    //Инициализируем и выделяем память под объект хеш-таблица типов объектов
    if (cityAndAreaAndType->exec("select  * from cities order by name asc"))//Запрашиваем ID и имя городов
    {
        while (cityAndAreaAndType->next())//Добавляем в нужный хеш каждый следующий элемент
            this->cities->insert(cityAndAreaAndType->value(0).toInt(),cityAndAreaAndType->value(1).toString());//
        if (cityAndAreaAndType->exec("select  * from areas order by name asc"))//Запрашиваем ID и имя районов
        {
            while (cityAndAreaAndType->next())//Добавляем в нужный хеш каждый следующий элемент
                this->areas->insert(cityAndAreaAndType->value(0).toInt(),cityAndAreaAndType->value(1).toString());//

            if (cityAndAreaAndType->exec("select  * from typeOfObjects order by name asc"))//Запрашиваем ID и имя типа объекта
            {
                while (cityAndAreaAndType->next())//Добавляем в нужный хеш каждый следующий элемент
                    this->typeOfObject->insert(cityAndAreaAndType->value(0).toInt(),cityAndAreaAndType->value(1).toString());//
            }
            else
                this->DBError();//Иначе выдаём предупреждение всплывающем окном
        }
        else
            this->DBError();//Иначе выдаём предупреждение всплывающем окном
    }
    else
        this->DBError();//Иначе выдаём предупреждение всплывающем окном

    this->loginForm = new LoginWidget(this->baseWidget);
    //Инициализируем и выделяем память под объект формочки авторизации
    connect(loginForm,SIGNAL(loginAccepted()),
            this, SLOT(slotLoginFormAccepted()));
    //Соединяем сигнал и слот успешного ввода логин-пароль
    showWidget(this->loginForm);//Отображаем форму
}

ControlViewWidget::~ControlViewWidget()
{

}
//
/*
 * Далее пойдут слоты для кнопок из главного меню, в целом всё похоже.
 * Проверяем было ли создано окно ранее, если нет, то выделяем память показываем,
 * связываем нужные сигналы со слотам, иначе
 * показываем окно снова(вдруг свёрнуто) и делаем фокус на него.
 * Специфичные моменты прокомментирую
 */
void ControlViewWidget::slotEntryToListOfOwners()//Поиск владельцев
{
    if(this->listOfOwnersWidget == nullptr)
    {
        this->listOfOwnersWidget = new ListOfOwners;
        this->listOfOwnersWidget->show();
        connect(this->listOfOwnersWidget,SIGNAL(signalSearchOwner()),
                this, SLOT(slotSearchOwner()));//Сигнал поиска владельца
    }
    else
    {
        this->listOfOwnersWidget->showNormal();
        QApplication::setActiveWindow(this->listOfOwnersWidget);
    }
}

void ControlViewWidget::slotEntryToListOfObjects()//Поиск объектов
{
    if(this->listOfObjectsWidget == nullptr)
    {
        this->listOfObjectsWidget = new ListOfObjects(0, this->cities, this->areas, this->typeOfObject);
        //Передаём в класс указатели на хэш-таблицы городов, районов и типов объектов.
        //Это нужно для заполнения комбобоксов на форме
        this->listOfObjectsWidget->show();
        connect(this->listOfObjectsWidget,SIGNAL(signalSearchObject()),
                this, SLOT(slotSearchObject()));//Сигнал поиска объекта
    }
    else
    {
        this->listOfObjectsWidget->showNormal();
        QApplication::setActiveWindow(this->listOfObjectsWidget);
    }
}

void ControlViewWidget::slotEntryToAddOwner()//Добавление владельца
{
    if(this->addNewOwner == nullptr)
    {
        this->addNewOwner = new AddNewOwnersWidget;
        this->addNewOwner->show();
        connect(addNewOwner,SIGNAL(signalAdd()),
                this,SLOT(slotAddOwner()));//Сигнал добавления владельца
    }
    else
    {
        this->addNewOwner->showNormal();
        QApplication::setActiveWindow(this->addNewOwner);
    }
}

void ControlViewWidget::slotEntryToAddObject()//Добавление объекта
{
    QHash <int, QString> *fio = new QHash <int, QString>;
    QSqlQuery queryFio;

    queryFio.exec("select id,surName|| ' ' ||firstName|| ' ' ||fatherName,old  from owners");
    while (queryFio.next())
    {
        fio->insert(queryFio.value(0).toInt(), queryFio.value(1).toString() + " Возраст: " +queryFio.value(2).toString());
    }
    /*
      *  Получаем список владельцев и передаём в функция обновления выпадающего списка.
    */

    if (this->addNewObject == nullptr)
    {
        this->addNewObject = new AddNewObjectWidget(0, fio,  this->cities, this->areas, this->typeOfObject);
        this->addNewObject->show();
        connect(this->addNewObject, SIGNAL(signalAdd()),
                this,SLOT(slotAddObject()));//Сигнал добавления объекта
    }
    else
    {
        this->addNewObject->setComboBoxFio(fio);
        this->addNewObject->showNormal();
        QApplication::setActiveWindow(this->addNewObject);
    }
}

void ControlViewWidget::slotEntryToEditOwner()//Редактирование владельца
{
    if (this->editOwner == nullptr)
    {
        this->editOwner = new EditOwnerWidget;
        this->editOwner->show();
        connect(this->editOwner,SIGNAL(signalEdit()),
                this, SLOT(slotEditOwner()));//Сигнал редактирования владельца
        connect(this->editOwner,SIGNAL(signalQueryOfData()),
                this, SLOT(slotQueryOfDataOwner()));//Сигнал получения владельца
        connect(this->editOwner,SIGNAL(signalRemove()),
                this, SLOT(slotRemoveOwner()));//Сигнал удаления владельца
    }
    else
    {
        this->editOwner->showNormal();
        QApplication::setActiveWindow(this->editOwner);
    }
}

void ControlViewWidget::slotEntryToEditObject()//Редактирования объекта
{
    if(this->editObject == nullptr)
    {
        this->editObject = new EditObjectWidget(0, this->cities, this->areas, this->typeOfObject);
        //Передаём в конструктор указатели на список городов, районов и типо объектов
        this->editObject->show();
        connect(this->editObject,SIGNAL(signalEdit()),
                this, SLOT(slotEditObject()));//Сигнал редактирования текущего объекта
        connect(this->editObject, SIGNAL(signalQueryOfData()),
                this,SLOT(slotQueryOfDataObject()));//Сигнал получения данных для объекта по ID
        connect(this->editObject, SIGNAL(signalRemove()),
                this,SLOT(slotRemoveObject()));//Сигнал удаление объекта
    }
    else
    {
        this->editObject->showNormal();
        QApplication::setActiveWindow(this->editObject);
    }
}

void ControlViewWidget::slotEntryToSettings()//Настройки
{
    if(this->userSettings == nullptr)
    {
        this->userSettings = new SettingsWidget;
        this->userSettings->show();
        connect(this->userSettings, SIGNAL(signalEditSettings()),
                this,SLOT(slotEditSettings()));//
        connect(this->userSettings, SIGNAL(signalQueryOfData()),
                this, SLOT(slotQueryOfDataSettings()));//
        slotQueryOfDataSettings();//
    }
    else
    {
        this->userSettings->showNormal();//
        QApplication::setActiveWindow(this->userSettings);//
    }
}

void ControlViewWidget::slotEntryToAdmin()//Админ-панель
{

    if(this->adminPanel == nullptr)
    {
        this->adminPanel = new AdminPanelWidget;
        this->adminPanel->show();
        connect(this->adminPanel, SIGNAL(signalAddOrEdit()),
                this, SLOT(slotAddOrEditWorker()));//Сигнал изменения или удаления работника
        connect(this->adminPanel, SIGNAL(signalQueryOfData()),
                this, SLOT(slotQueryOfDataWorker()));//Сигнал получения данных работника
        connect(this->adminPanel, SIGNAL(signalRemove()),
                this, SLOT(slotRemoveWorker()));//Сигнал удаления работника
        connect(this->adminPanel, SIGNAL(signalShowWorkers(int)),
                this, SLOT(slotShowWorkers(int)));//Сигнал отображения работников
    }
    else
    {
        this->adminPanel->showNormal();
        QApplication::setActiveWindow(this->adminPanel);
    }
}
//
void ControlViewWidget::slotLoginFormAccepted()
{
    AccountVerification currentVerAcc;//Класс авторизации
    connect(&currentVerAcc,SIGNAL(signalDBError()),
            this, SLOT(DBError()));//Сигнал ошибки получения данных
    currentVerAcc.setLogin(loginForm->getLogin());//Передаём логин
    currentVerAcc.setPassword(loginForm->getPassword());//Передаём пароль
    if (currentVerAcc.auth())
    {
        //Авторизация прошла
        this->id = currentVerAcc.getId();//Получаем ID пользователя
        this->typeUser = currentVerAcc.getTypeUser();//Получаем тип пользователя
        this->userWidget = new UserFormWidget(this->baseWidget, this->typeUser);
        //Инициализируем и выделяем память под объект главного меню

        connect(userWidget,SIGNAL(signalEntryToListOfObjects()),
                this,SLOT(slotEntryToListOfObjects()));//Поиск владельцев
        connect(userWidget,SIGNAL(signalEntryToListOfOwners()),
                this,SLOT(slotEntryToListOfOwners()));//Поиск объектов
        connect(userWidget,SIGNAL(signalEntryToAddOwner()),
                this,SLOT(slotEntryToAddOwner()));//Добавление владельца
        connect(userWidget,SIGNAL(signalEntryToAddObject()),
                this,SLOT(slotEntryToAddObject()));//Добавление объекта
        connect(userWidget,SIGNAL(signalEntryToEditOwner()),
                this,SLOT(slotEntryToEditOwner()));//Редактирование владельца
        connect(userWidget,SIGNAL(signalEntryToEditObject()),
                this,SLOT(slotEntryToEditObject()));//Редактирования объекта
        connect(userWidget,SIGNAL(signalEntryToSettings()),
                this,SLOT(slotEntryToSettings()));//Настройки
        connect(userWidget,SIGNAL(signalEntryToAdmin()),
                this,SLOT(slotEntryToAdmin()));//Админ-панель

        showWidget(this->userWidget);//Показываем главное окно
        changeNameUser();//Меняем имя в UserWidget

    }
    else
    {
        //Не прошла
        this->loginForm->wrongAcc();//Метод по отображению неправильного ввода логин-пароль
    }
}
//
void ControlViewWidget::slotAddOwner()//Добавление владельца
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//Инициализация объекта для хранения данных с формы
    this->addNewOwner->getInfoWithForm(dataForm);//Получение данных с формы
    QSqlQuery queryAddOwner;//Объект запроса для добавления владельцев
    bool reply = false;//флажок состояния запроса
    QString queryAddOwnerText;//Строка запроса на добавление
    queryAddOwnerText.append("INSERT INTO owners "
                             "(surName, firstName, fatherName ,old, sex, phone, desc, worker) "
                             "VALUES ('%1', '%2', '%3', %4, %5, '%6', '%7', %8)");
    queryAddOwnerText = queryAddOwnerText.arg(dataForm->value("surName").toString())
            .arg(dataForm->value("firstName").toString()).arg(dataForm->value("fatherName").toString())
            .arg(dataForm->value("old").toString()).arg(dataForm->value("sex").toString())
            .arg(dataForm->value("phone").toString()).arg(dataForm->value("desc").toString()).arg(QString::number(this->id));
    reply = queryAddOwner.exec(queryAddOwnerText);//делаем запрос в нашу бд и получаем ответ
    if(reply)
    {
        emit this->addNewOwner->signalSuccess();//Успешное добавление овнера
    }
    else
        this->DBError(this->addNewOwner, "Не удалось добавить владельца");//Отображаем ошибку
}

void ControlViewWidget::slotEditOwner()//Редактирование владельца
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;
    int idOwner = this->editOwner->getIdWithForm();
    QSqlQuery queryEditOwner;//Объект запроса редактирования владельца
    bool reply = false;
    QString queryEditOwnerText;

    queryEditOwnerText =
            "select count(id) from owners where id = " +
            QString::number(idOwner);//Формируем запрос для проверки существует ли редактируемый владелец

    if (this->typeUser == 0) queryEditOwnerText +=
            " and worker = " + QString::number(this->id);
    //если юзер не админ, то добавляем к запросу параметр worker, равный ID текущего пользователя

    qDebug() << queryEditOwnerText;//Выводим в консоль дебага результирующий запрос
    reply = queryEditOwner.exec(queryEditOwnerText);//
    if(reply)
    {
        queryEditOwner.next();//
        if (queryEditOwner.record().value(0) == 0)
            return;//
    }
    else
    {
        this->DBError();//
        return;//
    }
    queryEditOwnerText.clear();//

    this->editOwner->getInfoWithForm(dataForm);//
    queryEditOwnerText = QString("update owners set surName = '%1', firstName = '%2', "
                                 "fatherName = '%3', old = %4, sex = %5, phone = '%6', desc = '%7' where id = %8 ")
            .arg(dataForm->value("surName").toString()).arg(dataForm->value("firstName").toString())
            .arg(dataForm->value("fatherName").toString()).arg(dataForm->value("old").toString())
            .arg(dataForm->value("sex").toString()).arg( dataForm->value("phone").toString())
            .arg(dataForm->value("desc").toString()).arg(idOwner);//
    if (this->typeUser == 0)
        queryEditOwnerText.append("and worker =  " + QString::number(this->id));//
    qDebug() << queryEditOwnerText;//
    reply = queryEditOwner.exec(queryEditOwnerText);//
    if(reply)
        emit this->editOwner->signalSuccess();//
    else
        this->DBError();//
}

void ControlViewWidget::slotRemoveOwner()//Удаление владельца
{
    int idOwner = this->editOwner->getIdWithForm();//
    QSqlQuery queryEditOwner;//
    bool reply = false;//
    QString queryEditOwnerText;//


    queryEditOwnerText =
            "select count(id) from owners where id = " +
            QString::number(idOwner);//

    if (this->typeUser == 0) queryEditOwnerText +=
            " and worker = " + QString::number(this->id);//

    qDebug() << "remove" << queryEditOwnerText;//
    reply = queryEditOwner.exec(queryEditOwnerText);//
    if(reply)
    {
        queryEditOwner.next();//
        if (queryEditOwner.record().value(0) == 0)
            return;//
    }
    else
    {
        this->DBError();//
        return;//
    }
    queryEditOwnerText.clear();//

    int messReply = QMessageBox::information(
                this->editOwner,
                "Удаление объекта",
                "Вы действительно хотите удалить владельца с ID:"
                + QString::number(this->editOwner->getIdWithForm()) + "?", QMessageBox::Ok | QMessageBox::Cancel);//
    if (messReply == QMessageBox::Ok)
    {
        queryEditOwnerText =
                "select count(id) from saleOfTikets where owner = " +
                QString::number(idOwner);//
        qDebug() << queryEditOwnerText;//
        reply = queryEditOwner.exec(queryEditOwnerText);//
        if(reply)
        {qDebug() << "remove";//
            queryEditOwner.next();//
            if (queryEditOwner.record().value(0) == 0)
            {
                queryEditOwnerText =
                        "select count(id) from rentOfTikets where owner = " +
                        QString::number(idOwner);//
                qDebug() << queryEditOwnerText;//
                reply = queryEditOwner.exec(queryEditOwnerText);//
                if(reply)
                {qDebug() << "remove";//
                    queryEditOwner.next();//
                    if (queryEditOwner.record().value(0) == 0)
                    {
                        queryEditOwnerText = "delete from owners where id = " + QString::number(idOwner);//
                        qDebug() << queryEditOwnerText;//
                        reply = queryEditOwner.exec(queryEditOwnerText);//
                        if(reply)
                        {qDebug() << "remove";//
                            this->editOwner->signalSuccessRemove();//
                        }
                        else
                            this->DBError();//
                    }
                    else
                        this->DBError(this->editOwner,
                                      "У пользователя есть свои объекты.\nДля удаления пользователя у него не должно быть объектов");//
                }
                else
                {
                    this->DBError();//
                }
            }
            else
            {
                this->DBError(this->editOwner,
                              "У пользователя есть свои объекты.\nДля удаления пользователя у него не должно быть объектов");//
            }
        }
        else
        {
            this->DBError();//
        }
    }
}

void ControlViewWidget::slotSearchOwner()//Поиск владельца
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//
    this->listOfOwnersWidget->getInfoWithForm(dataForm);//
    QSqlQueryModel *querySearchObjectModel = new QSqlQueryModel();//
    QString querySearchObjectText;//

    querySearchObjectText.append(
                "select "
                "owners.id AS 'ID', "
                "owners.surName AS 'Фамилия', owners.firstName AS 'Имя', owners.fatherName AS 'Отчество', "
                "owners.old AS 'Возраст', owners.sex AS 'Пол', owners.phone AS 'Телефон', "
                "owners.desc AS 'Примечания', workers.firstName||' '||workers.surName||' '||workers.fatherName AS 'Работник' "
                "from "
                "owners, workers "
                "where "
                "owners.worker = workers.id ");//

    this->insertWhereInSqlQuery(&querySearchObjectText, "owners.surName", dataForm->value("surName").toString());//
    this->insertWhereInSqlQuery(&querySearchObjectText, "owners.firstName", dataForm->value("firstName").toString());//
    this->insertWhereInSqlQuery(&querySearchObjectText, "owners.fatherName", dataForm->value("fatherName").toString());//
    this->insertWhereInSqlQuery(&querySearchObjectText, "owners.phone", dataForm->value("phone").toString());//
    this->insertWhereInSqlQuery(&querySearchObjectText, "owners.sex", dataForm->value("sex"));//

    if (!dataForm->value("anyOld").toBool())//Поставлена ли галочка "Любой возраст"?
        this->insertWhereInSqlQuery(&querySearchObjectText, "owners.old", dataForm->value("old"));//

    if (dataForm->value("listMyRecord").toBool())//Поставлена ли галочка "Показать мои записи"?
        querySearchObjectText = querySearchObjectText.append(" and owners.worker = " + QString::number(this->id));//

    qDebug() << querySearchObjectText;//

    querySearchObjectModel->setQuery(querySearchObjectText);//
    this->userWidget->updateTableOfListOwnersOrObjects(querySearchObjectModel);//

    if (querySearchObjectModel->query().isSelect())
        emit this->listOfOwnersWidget->signalSuccess();//
    else
        this->DBError();//
}

void ControlViewWidget::slotQueryOfDataOwner()//Получение данных владельца
{
    QHash<QString, QVariant> *dataForm = nullptr;//
    QSqlQuery dataOfOwnerQuery;//
    QString dataOfOwnerQueryText;//
    bool reply = false;//
    int idOwner = this->editOwner->getIdWithForm();//


    if (this->typeUser == 0)
    {
        dataOfOwnerQueryText =
                "select count(id) from owners where id = " +
                QString::number(idOwner) +
                " and worker = " +
                QString::number(this->id);//

        qDebug() << "typeUser" << dataOfOwnerQueryText;//
        reply = dataOfOwnerQuery.exec(dataOfOwnerQueryText);//
        if(reply)
        {
            dataOfOwnerQuery.next();//
            if (dataOfOwnerQuery.record().value(0) == 0)
                return;//
        }
        else
        {
            this->DBError(this->editOwner);//
            return;//
        }
        dataOfOwnerQueryText.clear();//
        reply = false;//
    }

    dataOfOwnerQueryText.append(
                "from "
                "owners "
                "where "
                "id =  " +
                QString::number(idOwner));//
    if (this->typeUser == 0)
        dataOfOwnerQueryText.append(" and worker = " + QString::number(this->id));//
    reply = dataOfOwnerQuery.exec("select count(id) " + dataOfOwnerQueryText);//
    if(reply)
    {
        dataOfOwnerQuery.next();//
        if (dataOfOwnerQuery.record().value(0) > 0)
        {
            reply = dataOfOwnerQuery.exec("select  surName, firstName, fatherName, phone, sex, old, desc " + dataOfOwnerQueryText);//
            if(reply)
            {
                dataOfOwnerQuery.next();//
                dataForm = new QHash<QString, QVariant>;//
                dataForm->insert("surName", dataOfOwnerQuery.record().value(0));//
                dataForm->insert("firstName",  dataOfOwnerQuery.record().value(1));//
                dataForm->insert("fatherName",  dataOfOwnerQuery.record().value(2));//
                dataForm->insert("phone",  dataOfOwnerQuery.record().value(3));//
                dataForm->insert("sex",  dataOfOwnerQuery.record().value(4));//
                dataForm->insert("old",  dataOfOwnerQuery.record().value(5));//
                dataForm->insert("desc",  dataOfOwnerQuery.record().value(6));//
                this->editOwner->setInfoIntoForm(dataForm);//
            }
            else
                this->DBError(this->editOwner);//
        }
        else
        {

            if (this->typeUser == 0)
                this->DBError(this->editOwner,
                              "Данного ID нет, либо вы ошиблись при вводе.\nВы можете изменить только владельцев, добавленных вами.");//
            else
                this->DBError(this->editOwner, "Данного ID нет, либо вы ошиблись при вводе.");//
        }
    }
    else
        this->DBError(this->editOwner);//
}

void ControlViewWidget::slotAddObject()//Добавить объект
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//
    this->addNewObject->getInfoWithForm(dataForm);//

    QSqlQuery queryAddObject;//
    QString queryAddObjectText;
    bool reply = false;//

    queryAddObjectText =
            "select count(id) from owners where id = " +
            QString::number(dataForm->value("idOfOwner").toInt());//

    if (this->typeUser == 0) queryAddObjectText +=
            " and worker = " + QString::number(this->id);//

    qDebug() << "remove" << queryAddObjectText;//
    reply = queryAddObject.exec(queryAddObjectText);//
    if(reply)
    {
        queryAddObject.next();//
        if (queryAddObject.record().value(0) == 0)
        {
            this->DBError(this->addNewObject, "Не найден владелец");//
            return;//
        }
    }
    else
    {
        this->DBError(this->addNewObject);//
        return;//
    }
    queryAddObjectText.clear();//



    if (dataForm->value("street").toString().length()<2)
    {
        this->DBError(this->addNewObject, "Введите улицу");
        return;
    }
    //Получаем данные с формы
    /*QHash<QString, QString> dataForm;//
     * [searchSaleOrRent] = Sale/Rent
     * [selectTypeOfObject] = комната=0, квартира=1, дом с участком=2, дом без участка=3, участок=4
     *[area] = 0... (выборка из таблицы area)
     *[city] = 0... (выборка из таблицы city)
     *[needInComfort] = 0/1
     *[needInBalcony] = 0/1
     *[needInLoggia] = 0/1
     *[countOfStoreys] = 1...
     *[countOfRooms] = 1...
     *[fioOfOwner] = 1...
     *[street] = abc...
     *[numberHouse] = 1...
     *[numberApartment] = 1...
     *[square] = 1...
     *[priceOfSale] = 1...
     **[desc] = abc....
     *For rent
     **[timeOfRent] = 1... (выборка из таблицы timeOfRent)
     **[accommodationWithAOwner] = 1/0
     **[countRoomsOfRent] = 1...
     **[priceOfRent] = 1...
     */

    queryAddObjectText = "INSERT INTO ";//
    int type = dataForm->value("searchSaleOrRent").toInt();//
    if (type == 0)
    {
        queryAddObjectText.append("saleOfTikets "
                                  "(typeOfObject, numberHouse, numberApartment, street, square, needInComfort, needInBalcony, "
                                  "needInLoggia, countOfStoreys, countOfRooms, priceOfSale, desc, owner, city, area, worker) "
                                  "VALUES (%1,%2,%3,'%4',%5,%6,%7,%8,%9,%10,%11,'%12',%13,%14,%15,%16)");//

        queryAddObjectText = queryAddObjectText
                .arg(dataForm->value("typeOfObject").toInt()).arg(dataForm->value("numberHouse").toInt())
                .arg(dataForm->value("numberApartment").toInt()).arg(dataForm->value("street").toString())
                .arg(dataForm->value("square").toInt()).arg(dataForm->value("needInComfort").toBool())
                .arg(dataForm->value("needInBalcony").toBool()).arg(dataForm->value("needInLoggia").toBool())
                .arg(dataForm->value("countOfStoreys").toInt()).arg(dataForm->value("countOfRooms").toInt())
                .arg(dataForm->value("priceOfSale").toInt()).arg(dataForm->value("desc").toString())
                .arg(dataForm->value("idOfOwner").toInt()).arg(dataForm->value("city").toInt())
                .arg(dataForm->value("area").toInt()).arg(QString::number(this->id));//
    }
    if (type == 1)
    {
        queryAddObjectText.append("rentOfTikets "
                                  "(typeOfObject, numberHouse, numberApartment, street, square, needInComfort, needInBalcony, "
                                  "needInLoggia, countOfStoreys, countOfRooms, countOfRoomsUnderRent, "
                                  "accommodationWithAOwner, priceOfRent, timeOfRent, desc, owner, city, area, worker) "
                                  "VALUES (%1,%2,%3,'%4',%5,%6,%7,%8,%9,%10,%11,%12,%13,%14,'%15',%16,%17,%18,%19)");//

        queryAddObjectText = queryAddObjectText
                .arg(dataForm->value("typeOfObject").toInt()).arg(dataForm->value("numberHouse").toInt())
                .arg(dataForm->value("numberApartment").toInt()).arg(dataForm->value("street").toString())
                .arg(dataForm->value("square").toInt()).arg(dataForm->value("needInComfort").toBool())
                .arg(dataForm->value("needInBalcony").toBool()).arg(dataForm->value("needInLoggia").toBool())
                .arg(dataForm->value("countOfStoreys").toInt()).arg(dataForm->value("countOfRooms").toInt())
                .arg(dataForm->value("countOfRoomsUnderRent").toInt()).arg(dataForm->value("accommodationWithAOwner").toBool())
                .arg(dataForm->value("priceOfRent").toInt()).arg(dataForm->value("timeOfRent").toInt())
                .arg(dataForm->value("desc").toString()).arg(dataForm->value("idOfOwner").toInt())
                .arg(dataForm->value("city").toInt()).arg(dataForm->value("area").toInt())
                .arg(QString::number(this->id));//;//
    }

    qDebug() << queryAddObjectText;//
    reply = queryAddObject.exec(queryAddObjectText);//

    if(reply)
    {
        this->addNewObject->signalSuccess();//
    }
    else
        this->DBError();//
}

void ControlViewWidget::slotEditObject()//Редактировать объект
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//

    //Получаем данные с формы
    /*QHash<QString, QString> dataForm;//
     * [searchSaleOrRent] = Sale/Rent
     * [selectTypeOfObject] = комната=0, квартира=1, дом с участком=2, дом без участка=3, участок=4
     *[area] = 0... (выборка из таблицы areas)
     *[city] = 0... (выборка из таблицы cities)
     *[needInComfort] = 0/1
     *[needInBalcony] = 0/1
     *[needInLoggia] = 0/1
     *[countOfStoreys] = 1...
     *[countOfRooms] = 1...
     *[fioOfOwner] = 1...
     *[street] = abc...
     *[numberHouse] = 1...
     *[numberApartment] = 1...
     *[square] = 1...
     *[priceOfSale] = 1...
     **[desc] = abc....
     *For rent
     **[timeOfRent] = 1... (выборка из таблицы timeOfRent)
     **[accommodationWithAOwner] = 1/0
     **[countRoomsOfRent] = 1...
     **[priceOfRent] = 1...
     */

    QSqlQuery queryEditObject;//
    QString queryEditObjectText;//
    bool reply = false;//

    int typeTicket = this->editObject->getTypeTicketWithForm();//

    queryEditObjectText =
            "select count(id) from owners where id = " +
            QString::number(dataForm->value("idOfOwner").toInt());//

    if (this->typeUser == 0) queryEditObjectText +=
            " and worker = " + QString::number(this->id);//

    qDebug() << "remove" << queryEditObjectText;//
    reply = queryEditObject.exec(queryEditObjectText);//
    if(reply)
    {
        queryEditObject.next();//
        if (queryEditObject.record().value(0) == 0)
        {
            this->DBError(this->editObject, "Не найден владелец");//
            return;//
        }
    }
    else
    {
        this->DBError(this->addNewObject);//
        return;//
    }
    queryEditObjectText.clear();//

    queryEditObjectText = "select count(id) from ";//
    if (typeTicket == 1)
        queryEditObjectText.append("saleOfTikets ");//
    if (typeTicket == 2)
        queryEditObjectText.append("rentOfTikets ");//
    queryEditObjectText += "where id = " +
            QString::number(this->editObject->getIdWithForm());//
    if (this->typeUser == 0)
        queryEditObjectText += " and worker = " +  QString::number(this->id);//
    qDebug() << queryEditObjectText;//
    reply = queryEditObject.exec(queryEditObjectText);//
    if(reply)
    {
        queryEditObject.next();//
        if (queryEditObject.record().value(0) == 0)
            return;//
    }
    else
    {
        this->DBError();//
        return;//
    }
    queryEditObjectText.clear();//

    if (typeTicket == 1)
        queryEditObjectText.append("update saleOfTikets set ");//
    if (typeTicket == 2)
        queryEditObjectText.append("update rentOfTikets set ");//

    this->editObject->getInfoWithForm(dataForm);//
    queryEditObjectText.append(
                "typeOfObject = %1 , numberHouse = %2, numberApartment = %3, street = '%4', square = %5, "
                "needInComfort = %6, needInBalcony = %7, needInLoggia = %8, countOfStoreys = %9, "
                "countOfRooms = %10, desc = '%11', owner = %12, city = %13, area = %14 ");//
    queryEditObjectText = queryEditObjectText
            .arg(dataForm->value("typeOfObject").toInt()).arg(dataForm->value("numberHouse").toInt())
            .arg(dataForm->value("numberApartment").toInt()).arg(dataForm->value("street").toString())
            .arg(dataForm->value("square").toInt())
            .arg(dataForm->value("needInComfort").toBool()).arg(dataForm->value("needInBalcony").toBool())
            .arg(dataForm->value("needInLoggia").toBool())
            .arg(dataForm->value("countOfStoreys").toInt()).arg(dataForm->value("countOfRooms").toInt())
            .arg(dataForm->value("desc").toString())
            .arg(dataForm->value("owner").toInt()).arg(dataForm->value("city").toInt())
            .arg(dataForm->value("area").toInt());//

    if (typeTicket == 1)
    {
        queryEditObjectText.append(", priceOfSale = %1 ");//
        queryEditObjectText = queryEditObjectText
                .arg(dataForm->value("priceOfSale").toInt());//
    }
    if (typeTicket == 2)
    {
        queryEditObjectText.append(", countOfRoomsUnderRent = %1, accommodationWithAOwner = %2, "
                                   "priceOfRent = %3, timeOfRent = %4 ");//
        queryEditObjectText = queryEditObjectText
                .arg(dataForm->value("countOfRoomsUnderRent").toInt())
                .arg(dataForm->value("accommodationWithAOwner").toBool())
                .arg(dataForm->value("priceOfRent").toInt()).arg(dataForm->value("timeOfRent").toInt());//
    }
    queryEditObjectText.append("where id = " +  QString::number(this->editObject->getIdWithForm()));//
    if (this->typeUser == 0)
        queryEditObjectText.append(" and worker = " +  QString::number(this->id));//

    qDebug() << queryEditObjectText;//
    reply = queryEditObject.exec(queryEditObjectText);//
    if(reply)
    {
        emit this->editObject->signalSuccess();//
    }
    else
        this->DBError();//

}

void ControlViewWidget::slotRemoveObject()//Удалить объект
{
    QSqlQuery queryRemoveObject;//
    QString queryRemoveObjectText;//
    bool reply = false;//
    int idEditObject = this->editObject->getIdWithForm();//
    int type = this->editObject->getTypeTicketWithForm();//

    queryRemoveObjectText = "select count(id) from ";//
    if (type == 1)
        queryRemoveObjectText.append("saleOfTikets ");//
    if (type == 2)
        queryRemoveObjectText.append("rentOfTikets ");//
    queryRemoveObjectText += "where id = " +
            QString::number(this->editObject->getIdWithForm());//
    if (this->typeUser == 0)
        queryRemoveObjectText += " and worker = " +  QString::number(this->id);//
    qDebug() << queryRemoveObjectText;//
    reply = queryRemoveObject.exec(queryRemoveObjectText);//
    if (reply)
    {
        queryRemoveObject.next();//
        if (queryRemoveObject.record().value(0) == 0)
        {
            this->DBError();//
            return;//
        }
    }
    else
    {
        this->DBError();//
        return;//
    }
    queryRemoveObjectText.clear();//

    int messReply = QMessageBox::information(
                this->editObject,
                "Удаление объекта",
                "Вы действительно хотите удалить объект с ID: "
                + QString::number(idEditObject) + "?", QMessageBox::Ok | QMessageBox::Cancel);//
    if (messReply == QMessageBox::Ok)
    {
        queryRemoveObjectText = "delete from ";//

        if (type == 1)
            queryRemoveObjectText.append("saleOfTikets ");//
        if (type == 2)
            queryRemoveObjectText.append("rentOfTikets ");//
        queryRemoveObjectText.append("where id = " + QString::number(idEditObject));//
        if (this->typeUser == 0)
            queryRemoveObjectText.append(" and worker =  " + QString::number(this->id));//
        qDebug() << queryRemoveObjectText;//
        reply = queryRemoveObject.exec(queryRemoveObjectText);//
        if(reply)
        {
            this->editObject->signalSuccessRemove();//
        }
        else
            this->DBError();//
    }
}

void ControlViewWidget::slotSearchObject()//Поиск объекта
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//
    QSqlQueryModel *querySearchObjectModel = new QSqlQueryModel();//
    QString querySearchObjectText;//
    this->listOfObjectsWidget->getInfoWithForm(dataForm);//
    int type = dataForm->value("searchSaleOrRent").toInt();//
    /*QHash<QString, QString> dataForm;//
     * [searchSaleOrRent] = Sale/Rent
     * [selectTypeOfObject] = Любой = 0 комната=1, квартира=2, дом с участком=3, дом без участка=4, участок=5
     *[area] = 0... (выборка из таблицы area)
     *[city] = 0... (выборка из таблицы city)
     *[needInComfort] = 0/1
     *[needInBalcony] = 0/1
     *[needInLoggia] = 0/1
     *[countOfStoreys] = 1...
     *[countOfRooms] = 1...
     *[fioOfOwner] = 1...
     *[street] = abc...
     *[numberHouse] = 1...
     *[numberApartment] = 1...
     *[square] = 1...
     *[priceOfSale] = 1...
     **[desc] = abc....
     *For rent
     **[timeOfRent] = 1... (выборка из таблицы timeOfRent)
     **[accommodationWithAOwner] = 1/0
     **[countRoomsOfRent] = 1...
     **[priceOfRent] = 1...
     */

    if (type == 0)
    {
        querySearchObjectText.append(
                    "select "
                    "saleOfTikets.id AS 'ID', "
                    "typeOfObjects.name AS 'Тип объекта' , "
                    "owners.surName AS 'Фамилия', owners.firstName AS 'Имя', owners.fatherName AS 'Отчество', "
                    "street AS 'Улица', numberHouse AS 'Номер дома', numberApartment AS 'Номер квартиры', square AS 'Площадь', "
                    "needInComfort AS 'Удобства', needInBalcony AS 'Балкон', needInLoggia AS 'Лоджия', "
                    "countOfStoreys AS 'Кол-во этажей', countOfRooms AS 'Кол-во комнат', "
                    "priceOfSale AS 'Цена продажи', saleOfTikets.desc AS 'Описание',cities.name AS 'Город', areas.name AS 'Район' "
                    "from "
                    "saleOfTikets,cities,areas,typeOfObjects,owners "
                    "where "
                    "saleOfTikets.city = cities.id and saleOfTikets.area = areas.id and saleOfTikets.owner = owners.id and saleOfTikets.typeOfObject = typeOfObjects.id ");//
    }
    if (type == 1)
    {
        querySearchObjectText.append(
                    "select "
                    "rentOfTikets.id AS 'ID', "
                    "typeOfObjects.name AS 'Тип объекта' , "
                    "owners.surName AS 'Фамилия', owners.firstName AS 'Имя', owners.fatherName AS 'Отчество', "
                    "street AS 'Улица', numberHouse AS 'Номер дома', numberApartment AS 'Номер квартиры', square AS 'Площадь', "
                    "needInComfort AS 'Удобства', needInBalcony AS 'Балкон', needInLoggia AS 'Лоджия', "
                    "countOfStoreys AS 'Кол-во этажей', countOfRooms AS 'Кол-во комнат', "
                    "countOfRoomsUnderRent AS 'Кол-во комнат под сдачу', "
                    "accommodationWithAOwner AS 'Проживание с хозяином', "
                    "priceOfRent AS 'Цена съёма', rentOfTikets.desc AS 'Описание',cities.name AS 'Город', areas.name AS 'Район' "
                    "from "
                    "rentOfTikets,cities,areas,typeOfObjects,owners "
                    "where "
                    "rentOfTikets.city = cities.id and rentOfTikets.area = areas.id and rentOfTikets.owner = owners.id and rentOfTikets.typeOfObject = typeOfObjects.id ");//
        this->insertWhereInSqlQuery(&querySearchObjectText, "accommodationWithAOwner", dataForm->value("accommodationWithAOwner"));//
        this->insertWhereInSqlQuery(&querySearchObjectText, "countOfRoomsUnderRent", dataForm->value("countRoomsOfRent"));//
    }

    this->insertWhereInSqlQuery(&querySearchObjectText, "typeOfObject", dataForm->value("typeOfObject"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "city", dataForm->value("city"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "area", dataForm->value("area"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "needInComfort", dataForm->value("needInComfort"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "needInBalcony", dataForm->value("needInBalcony"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "needInLoggia", dataForm->value("needInLoggia"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "countOfStoreys", dataForm->value("countOfStoreys"));//
    this->insertWhereInSqlQuery(&querySearchObjectText, "countOfRooms", dataForm->value("countOfRooms"));//

    qDebug() << dataForm->value("listMyRecord").toBool();
    if (dataForm->value("listMyRecord").toBool())
    {
        if (type == 0)
            querySearchObjectText = querySearchObjectText.append("and saleOfTikets.worker = " + QString::number(this->id));//
        if (type == 1)
            querySearchObjectText = querySearchObjectText.append("and rentOfTikets.worker = " + QString::number(this->id));//
    }

    qDebug() << querySearchObjectText;//

    querySearchObjectModel->setQuery(querySearchObjectText);//
    this->userWidget->updateTableOfListOwnersOrObjects(querySearchObjectModel);//

    if (querySearchObjectModel->query().isSelect())
    {
        emit this->listOfObjectsWidget->signalSuccess();//
        this->userWidget->updateTableOfListOwnersOrObjects(querySearchObjectModel);
    }
    else
        DBError(this->listOfObjectsWidget, "У вас нет объектов недвижимости.");
}

void ControlViewWidget::slotQueryOfDataObject()//Получение данных объекта
{
    QHash<QString, QVariant> *dataForm = nullptr;//
    QSqlQuery dataOfObjectQuery;//
    QString dataOfObjectQueryText;//
    bool reply = false;//
    int typeTicket = this->editObject->getTypeTicketWithForm();//

    if (typeTicket == 1)
    { //Поиск по продаже
        dataOfObjectQueryText.append(
                    "from "
                    "saleOfTikets "
                    "where "
                    "id = "
                    );//
    }
    if(typeTicket == 2)
    {//Поиск по аренде
        dataOfObjectQueryText.append(
                    "from "
                    "rentOfTikets "
                    "where "
                    "id = "
                    );//
    }

    dataOfObjectQueryText.append(QString::number(this->editObject->getIdWithForm()));//
    if (this->typeUser == 0)
        dataOfObjectQueryText.append(" and worker = " + QString::number(this->id));//
    qDebug() << dataOfObjectQueryText;//
    reply = dataOfObjectQuery.exec("select count(id) " + dataOfObjectQueryText);//
    if(reply)
    {
        qDebug() << dataOfObjectQuery.record();//
        dataOfObjectQuery.next();//
        qDebug() << dataOfObjectQuery.record();//
        if (dataOfObjectQuery.record().value(0) > 0)
        {
            reply = dataOfObjectQuery.exec("select * " + dataOfObjectQueryText);//
            if(reply)
            {
                dataForm = new QHash<QString, QVariant>;//
                QSqlRecord dataOfObjectQueryRecord = dataOfObjectQuery.record();//
                qDebug() << dataOfObjectQuery.record();//
                dataOfObjectQuery.next();//
                qDebug() << dataOfObjectQuery.record();//

                //dataForm->insert("", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("")));//

                dataForm->insert("typeOfObject", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("typeOfObject")));//
                dataForm->insert("area",  dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("area")));//
                dataForm->insert("city",   dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("city")));//
                dataForm->insert("needInComfort", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("needInComfort")));//
                dataForm->insert("needInBalcony",  dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("needInBalcony")));//
                dataForm->insert("needInLoggia",  dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("needInLoggia")));//
                dataForm->insert("numberApartment", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("numberApartment")));//
                dataForm->insert("countOfStoreys", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("countOfStoreys")));//
                dataForm->insert("countOfRooms",  dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("countOfRooms")));//
                dataForm->insert("numberHouse", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("numberHouse")));//
                dataForm->insert("square", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("square")));//
                dataForm->insert("street", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("street")));//
                dataForm->insert("idOfOwner", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("owner")));//
                dataForm->insert("desc", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("desc")));//

                if (typeTicket == 1) //Если продажа
                {
                    dataForm->insert("priceOfSale", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("priceOfSale")));//
                }
                if(typeTicket == 2)
                {
                    dataForm->insert("timeOfRent", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("timeOfRent")));//
                    dataForm->insert("accommodationWithAOwner",  dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("accommodationWithAOwner")));//
                    dataForm->insert("countOfStoreys", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("countOfStoreys")));//
                    dataForm->insert("priceOfRent", dataOfObjectQuery.value(dataOfObjectQueryRecord.indexOf("priceOfRent")));//
                }

                this->editObject->setInfoIntoForm(dataForm);//
            }
            else
                this->DBError();//
        }
        else
        {
            QString textError;//
            textError.append("Данного ID нет, либо вы ошиблись при вводе.");//
            if (this->typeUser == 0)
                textError.append("\nВы можете изменить только владельцев, добавленных вами.");//
            if (QMessageBox::warning(this->baseWidget,"Ничего не найдено", textError))
                QApplication::setActiveWindow(this->editObject);//
        }
    }
    else
        this->DBError();//
}

void ControlViewWidget::slotEditSettings()//Редактирование настроек
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//
    this->userSettings->getInfoWithForm(dataForm);//
    QSqlQuery queryEditSettings;//
    bool reply = false;//
    QString queryEditSettingsText;//

    queryEditSettingsText = QString("update workers set surName = '%1', firstName = '%2', "
                                    "fatherName = '%3', login = '%4' ")
            .arg(dataForm->value("surName").toString()).arg(dataForm->value("firstName").toString())
            .arg(dataForm->value("fatherName").toString()).arg(dataForm->value("login").toString());//
    this->insertWhereInSqlQuery(&queryEditSettingsText, "password", dataForm->value("password").toString());//
    queryEditSettingsText.append("where id = " + QString::number(this->id));//

    reply = queryEditSettings.exec(queryEditSettingsText);//
    if(reply)
        this->changeNameUser();//
    else
        this->DBError();//
    emit this->userSettings->signalSuccess();//
}

void ControlViewWidget::slotQueryOfDataSettings()//Получить данные настроек
{
    QHash<QString, QVariant> *dataForm = nullptr;//
    QSqlQuery dataOfSettingsQuery;//
    QString dataOfSettingsQueryText;//
    bool reply = false;//
    dataOfSettingsQueryText = "select * from workers where id = " + QString::number(this->id);//
    qDebug() << dataOfSettingsQueryText;//
    reply = dataOfSettingsQuery.exec(dataOfSettingsQueryText);//
    if(reply)
    {
        dataForm = new QHash<QString, QVariant>;//
        QSqlRecord dataOfSettingsQueryRecord = dataOfSettingsQuery.record();//
        dataOfSettingsQuery.next();//
        qDebug() << dataOfSettingsQuery.record();//
        dataForm->insert("login", dataOfSettingsQuery.value(dataOfSettingsQueryRecord.indexOf("login")));//
        dataForm->insert("password", "Скрыто");//
        dataForm->insert("surName", dataOfSettingsQuery.value(dataOfSettingsQueryRecord.indexOf("surName")));//
        dataForm->insert("firstName", dataOfSettingsQuery.value(dataOfSettingsQueryRecord.indexOf("firstName")));//
        dataForm->insert("fatherName", dataOfSettingsQuery.value(dataOfSettingsQueryRecord.indexOf("fatherName")));//
        this->userSettings->setInfoIntoForm(dataForm);//
    }
    else
        this->DBError(this->userSettings);//
}

void ControlViewWidget::slotAddOrEditWorker()//Добавление или редактирование работника
{
    QHash<QString, QVariant> *dataForm = new QHash<QString, QVariant>;//
    int id = this->adminPanel->getIdWithForm();//
    QSqlQuery queryAddOrEditWorker;//
    QString queryAddOrEditWorkerText;//
    bool reply = false;//
    QByteArray passwordByte;//
    QCryptographicHash hashPassword(QCryptographicHash::Md5);//


    this->adminPanel->getInfoWithForm(dataForm);//
    QString password = dataForm->value("password").toString();//
    qDebug() << id;
    qDebug() << password;
    if (id == 0)
    {

        if(password.length() != 12)
        {
            this->DBError(this->adminPanel, "Сгенерируйте пароль!");//
            return;//
        }
        else
        {
            passwordByte.append(password.toUtf8());//
            hashPassword.addData(passwordByte);//
        }
        //Добавление
        queryAddOrEditWorkerText = QString("insert into workers "
                                           "(surName, firstName, fatherName , login, password) "
                                           "VALUES ('%1', '%2', '%3', '%4', '%5')")
                .arg(dataForm->value("surName").toString()).arg(dataForm->value("firstName").toString())
                .arg(dataForm->value("fatherName").toString()).arg(dataForm->value("login").toString())
                .arg(QString(hashPassword.result().toHex()));//
        qDebug() << queryAddOrEditWorkerText;//
        reply = queryAddOrEditWorker.exec(queryAddOrEditWorkerText);//
        if(reply)
            emit this->adminPanel->signalSuccessAddOrEdit();//
        else
            this->DBError(this->adminPanel);//
        return;//
    }

    queryAddOrEditWorkerText =
            "select count(id) from workers where id = " +
            QString::number(id);//

    qDebug() << queryAddOrEditWorkerText;//
    reply = queryAddOrEditWorker.exec(queryAddOrEditWorkerText);//
    if(reply)
    {
        queryAddOrEditWorker.next();//
        if (queryAddOrEditWorker.record().value(0) == 0)
            return;//
    }
    else
    {
        this->DBError();//
        return;//
    }
    queryAddOrEditWorkerText.clear();//

    if(password.length() == 12)
    {
        passwordByte.append(password.toUtf8());//
        hashPassword.addData(passwordByte);//
    }
    //Изменение
    queryAddOrEditWorkerText = QString("update workers set "
                                       "surName = '%1', firstName = '%2', fatherName = '%3', login = '%4' ")
            .arg(dataForm->value("surName").toString()).arg(dataForm->value("firstName").toString())
            .arg(dataForm->value("fatherName").toString()).arg(dataForm->value("login").toString());//


    if(password.length() == 12)
    {

        queryAddOrEditWorkerText.append(", password = '" + QString(hashPassword.result().toHex()) + "' where id = " + QString::number(id));//
    }
    else
    {
        queryAddOrEditWorkerText.append("where id = " + QString::number(id));//
    }

    qDebug() << queryAddOrEditWorkerText;//
    reply = queryAddOrEditWorker.exec(queryAddOrEditWorkerText);//
    if(reply)
    {
        emit this->adminPanel->signalSuccessAddOrEdit();//
        this->changeNameUser();//
    }
    else
        this->DBError(this->adminPanel);//
}

void ControlViewWidget::slotQueryOfDataWorker()//Получение данных работника
{

    QHash<QString, QVariant> *dataForm = nullptr;//
    QSqlQuery QueryOfDataWorkerQuery;//
    QString QueryOfDataWorkerText;//
    bool reply = false;//
    QueryOfDataWorkerText = "select * from workers where id = " + QString::number(this->adminPanel->getIdWithForm());//
    qDebug() << QueryOfDataWorkerText;//
    reply = QueryOfDataWorkerQuery.exec(QueryOfDataWorkerText);//
    if(reply && !QueryOfDataWorkerQuery.record().isEmpty())
    {
        QueryOfDataWorkerQuery.next();//
        if (QueryOfDataWorkerQuery.value(QueryOfDataWorkerQuery.record().indexOf("login")).toString() == "")
        {
            this->DBError(this->adminPanel);//
            return;//
        }
        dataForm = new QHash<QString, QVariant>;//
        QSqlRecord QueryOfDataWorkerQueryRecord = QueryOfDataWorkerQuery.record();//
        qDebug() << QueryOfDataWorkerQuery.record();//
        dataForm->insert("login", QueryOfDataWorkerQuery.value(QueryOfDataWorkerQueryRecord.indexOf("login")));//
        dataForm->insert("surName", QueryOfDataWorkerQuery.value(QueryOfDataWorkerQueryRecord.indexOf("surName")));//
        dataForm->insert("firstName", QueryOfDataWorkerQuery.value(QueryOfDataWorkerQueryRecord.indexOf("firstName")));//
        dataForm->insert("fatherName", QueryOfDataWorkerQuery.value(QueryOfDataWorkerQueryRecord.indexOf("fatherName")));//
        this->adminPanel->setInfoIntoForm(dataForm);//
    }
    else
        this->DBError(this->adminPanel);//
}

void ControlViewWidget::slotRemoveWorker()//Удаление работника
{
    int idWorker = this->adminPanel->getIdWithForm();//
    QSqlQuery queryEditOwner;//
    bool reply = false;//
    QString queryEditOwnerText;//

    int messReply = QMessageBox::information(
                this->adminPanel,
                "Удаление объекта",
                "Вы действительно хотите удалить работника с ID:"
                + QString::number(idWorker) + "?", QMessageBox::Ok | QMessageBox::Cancel);//
    if (messReply == QMessageBox::Ok)
    {
        queryEditOwnerText =
                "select count(id) from saleOfTikets where worker = " +
                QString::number(idWorker);//
        qDebug() << queryEditOwnerText;//
        reply = queryEditOwner.exec(queryEditOwnerText);//
        if(reply)
        {
            qDebug() << "remove";//
            queryEditOwner.next();//
            if (queryEditOwner.record().value(0) == 0)
            {
                queryEditOwnerText =
                        "select count(id) from rentOfTikets where worker = " +
                        QString::number(idWorker);//
                qDebug() << queryEditOwnerText;//
                reply = queryEditOwner.exec(queryEditOwnerText);//
                if(reply)
                {
                    qDebug() << "remove";//
                    queryEditOwner.next();//
                    if (queryEditOwner.record().value(0) == 0)
                    {
                        queryEditOwnerText =
                                "select count(id) from owners where worker = " +
                                QString::number(idWorker);//
                        qDebug() << queryEditOwnerText;//
                        reply = queryEditOwner.exec(queryEditOwnerText);//
                        if(reply)
                        {
                            qDebug() << "remove";//
                            queryEditOwner.next();//
                            if (queryEditOwner.record().value(0) == 0)
                            {
                                queryEditOwnerText = "delete from workers where id = " + QString::number(idWorker);//
                                qDebug() << queryEditOwnerText;//
                                reply = queryEditOwner.exec(queryEditOwnerText);//
                                if(reply)
                                {
                                    qDebug() << "remove";//
                                    this->adminPanel->signalSuccessRemove();//
                                }
                                else
                                    this->DBError(this->adminPanel);//
                            }
                            else
                                this->DBError(this->adminPanel,
                                              "У работника есть владельцы объектов.\nДля удаления пользователя у него не должно быть владельцов объектов");//
                        }
                        else
                            this->DBError(this->adminPanel);//
                    }
                    else
                        this->DBError(this->adminPanel,
                                      "У работника есть объекты.\nДля удаления пользователя у него не должно быть объектов");//
                }
                else
                    this->DBError(this->adminPanel);//
            }
            else
                this->DBError(this->adminPanel,
                              "У пользователя есть свои объекты.\nДля удаления пользователя у него не должно быть объектов");//
        }
        else
            this->DBError(this->adminPanel);//
    }
}

void ControlViewWidget::slotShowWorkers(int index)//Показ работника
{
    if (index != 1) return;//
    QSqlQueryModel *queryShowWorkersModel = new QSqlQueryModel();//
    QString queryShowWorkersText;//

    queryShowWorkersText =
            "select workers.id AS 'ID', "
            "workers.surName AS 'Фамилия', workers.firstName AS 'Имя', "
            "workers.fatherName AS 'Отчество', workers.login AS 'Логин' "
            "from workers ";//
    qDebug() << queryShowWorkersText;//

    queryShowWorkersModel->setQuery(queryShowWorkersText);//
    this->adminPanel->updateTableOfListOwnersOrObjects(queryShowWorkersModel);//

    if (!queryShowWorkersModel->query().isSelect())
        this->DBError(this->adminPanel, "Ошибка выборки работников");//
}
//
void ControlViewWidget::DBError(QWidget *parentMessage, QString mess)//Ошибка БД
{
    /*
     * Метод вызываемый для вывода в сплывающем окно об ошибке подключения к БД,
     * либо ошибок с управлению(объекты, владельцы, работники)
     */
    parentMessage = parentMessage==nullptr ? this->baseWidget : parentMessage;//
    if (mess == NULL) mess = "Таких данныех не обнаружено.";//
    QMessageBox::warning(parentMessage, "Ошибка доступа", mess);//
}

void ControlViewWidget::slotCloseApplication()//Закрытие приложения
{
    if(this->listOfObjectsWidget != nullptr)
    {
        this->listOfObjectsWidget->close();//
    }
    if(this->listOfOwnersWidget != nullptr)
    {
        this->listOfOwnersWidget->close();//
    }
    if(this->addNewObject != nullptr)
    {
        this->addNewObject->close();//
    }
    if(this->addNewOwner != nullptr)
    {
        this->addNewOwner->close();//
    }
    if(this->editOwner != nullptr)
    {
        this->editOwner->close();//
    }
    if(this->editObject != nullptr)
    {
        this->editObject->close();//
    }
    if(this->userSettings != nullptr)
    {
        this->userSettings->close();//
    }
    if(this->adminPanel != nullptr)
    {
        this->adminPanel->close();//
    }
}

void ControlViewWidget::showWidget(QWidget *changeWidget)//Показ нужного виджета в главном окне
{
    this->baseWidget->setMinimumSize(changeWidget->minimumSize());//
    this->baseWidget->setMaximumSize(changeWidget->maximumSize());//
    QDesktopWidget *des = QApplication::desktop();//
    int x = (des->screen()->geometry().width() - changeWidget->width()) / 2;//
    int y = (des->screen()->geometry().height() - changeWidget->height()) / 2;//
    this->baseWidget->setGeometry(QRect(x, y, changeWidget->width(), changeWidget->height()));//
    if (lastWidget != nullptr)
        this->lastWidget->hide();//
    changeWidget->show();//
    this->lastWidget = changeWidget;//
}

void ControlViewWidget::changeNameUser()//Обновление имени в главном меню
{
    QSqlQuery changeInfoOfUserQuery;
    changeInfoOfUserQuery.exec(
                "SELECT surName, firstName, fatherName FROM workers WHERE id = "
                + QString::number(this->id));// формируем запрос для получения имени текущего пользователя
    changeInfoOfUserQuery.next();//
    QString info = QString("Здравствуйте, %1 \n %2 %3")
            .arg(changeInfoOfUserQuery.record().value(0).toString())
            .arg(changeInfoOfUserQuery.record().value(1).toString())
            .arg(changeInfoOfUserQuery.record().value(2).toString());
    userWidget->changeInfoOfUser(&info);
}
//
void ControlViewWidget::insertWhereInSqlQuery(QString *str, QString key, QVariant value)
{
    //функция добавляет в строку значение, если оно больше нуля
    if (value.toInt() > 0)
    {
        str->append("and " + key + " = %1 ");
        *str = str->arg(value.toString());
    }
}

void ControlViewWidget::insertWhereInSqlQuery(QString *str, QString key, QString value)
{
    //функция добавляет к строке строку, если она не пустая, и не равно маске номера телефона
    if(value != "" && value != " " and value != "+7 () -")
    {
        str->append("and " + key + " = '%1' ");
        *str = str->arg(value);
    }
}
