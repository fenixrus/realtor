#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QTimer>
#include "popupwidget.h"

namespace Ui {
class LoginWidget;
}

class LoginWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LoginWidget(QWidget *parent = 0);
    ~LoginWidget();
    QString getLogin();
    QString getPassword();
signals:
    void loginAccepted();
    void wrongAcc();

private slots:
    void slotWrongAcc();
    void slotWrongAccTimer();
    void checkNotEmptyForm();

private:
    Ui::LoginWidget *ui;
    QString styleDefaultInput;
};
#endif // LOGINWIDGET_H
