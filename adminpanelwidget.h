#ifndef ADMINPANELWIDGET_H
#define ADMINPANELWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QSqlQueryModel>
#include <QTimer>
#include <stdlib.h>
#include <ctime>

namespace Ui {
class AdminPanelWidget;
}

class AdminPanelWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AdminPanelWidget(QWidget *parent = 0);
    ~AdminPanelWidget();
    void setInfoIntoForm(QHash<QString, QVariant> *dataForm);
    void getInfoWithForm(QHash<QString, QVariant> *dataForm);
    void updateTableOfListOwnersOrObjects(QSqlQueryModel *model);
    int getIdWithForm();
signals:
    void signalAddOrEdit();
    void signalSuccessAddOrEdit();
    void signalRemove();
    void signalSuccessRemove();
    void signalQueryOfData();
    void signalShowWorkers(int index);
private slots:
    void slotAddedOrEdited();
    void slotSetDefaultStyleForAddOrEditButton();
    void slotRemoved();
    void slotSetDefaultStyleForRemoveButton();
    void slotReset();
    void slotGenerateNewPassword();
    void on_generateNewPassword_clicked();
    void on_idOfObject_valueChanged(int arg);
    void on_toolButton_pressed();
    void on_toolButton_released();

private:
    Ui::AdminPanelWidget *ui;
    QString styleDefaultAddOrEditButton;
    QString styleDefaultRemoveButton;
    QTimer *timerGenerateNewPassword;
    int countGenerateNewPasswordSymbol;
};

#endif // ADMINPANELWIDGET_H
