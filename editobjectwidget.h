#ifndef EDITOBJECTWIDGET_H
#define EDITOBJECTWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QTimer>

namespace Ui {
class EditObjectWidget;
}

class EditObjectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EditObjectWidget(QWidget *parent = nullptr,
                              QHash<int, QString> *cities = nullptr,
                              QHash<int, QString> *areas = nullptr,
                              QHash <int, QString> *typeOfObject = nullptr);
    ~EditObjectWidget();
    void setInfoIntoForm(QHash<QString, QVariant> *dataForm);
    void getInfoWithForm(QHash<QString, QVariant> *dataForm);
    int getTypeTicketWithForm();
    int getIdWithForm();
signals:
    void signalEdit();
    void signalRemove();
    void signalQueryOfData();
    void signalSuccessRemove();
    void signalSuccess();
private slots:
    void slotReset();
    void slotAdded();
    void slotRemoved();
    void slotSetDefaultStyleForEditButton();
    void slotSetDefaultStyleForRemoveButton();
    void on_searchSaleOrRent_currentIndexChanged(int index);

private:
    Ui::EditObjectWidget *ui;
    QString styleDefaultEditButton;
    QString styleDefaultRemoveButton;
    void selectsFill(QHash<int, QString> *data, QComboBox *select);
};

#endif // EDITOBJECTWIDGET_H
