#include "listofowners.h"
#include "ui_listofowners.h"

ListOfOwners::ListOfOwners(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ListOfOwners)
{
    ui->setupUi(this);
    connect(ui->buttonAdd, SIGNAL(clicked()),
            this,SIGNAL(signalSearchOwner()));
    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));
    connect(this,SIGNAL(signalSuccess()),
            this,SLOT(slotAdded()));
    ui->firstName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->surName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->fatherName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    this->styleDefaultAddButton = ui->buttonAdd->styleSheet();
}
//
ListOfOwners::~ListOfOwners()
{
    delete ui;
}
//
void ListOfOwners::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{
    dataForm->insert("listMyRecord", ui->listMyRecord->isChecked());
    dataForm->insert("surName", ui->surName->text());
    dataForm->insert("firstName", ui->firstName->text());
    dataForm->insert("fatherName", ui->fatherName->text());
    dataForm->insert("phone", ui->phone->text());
    dataForm->insert("sex", ui->sex->currentIndex());
    dataForm->insert("old", ui->old->value());
    dataForm->insert("anyOld",ui->anyOld->isChecked());
}

void ListOfOwners::slotReset()
{
    ui->listMyRecord->setChecked(false);
    ui->surName->clear();
    ui->firstName->clear();
    ui->fatherName->clear();
    ui->phone->clear();
    ui->sex->setCurrentIndex(0);
    ui->old->clear();
    ui->anyOld->setChecked(false);
}

void ListOfOwners::slotSetDefaultStyleForAddButton()
{
    ui->buttonAdd->setStyleSheet(this->styleDefaultAddButton);
}

void ListOfOwners::slotAdded()
{
    ui->buttonAdd->setStyleSheet(this->styleDefaultAddButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForAddButton()));

}
