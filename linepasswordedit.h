#ifndef QPASSWORDEDIT_H
#define QPASSWORDEDIT_H
#include <QDesktopWidget>
#include <QMouseEvent>
#include <QApplication>
#include <QClipboard>
#include <QLineEdit>
#include <QDebug>
#include <QTimer>
#include "popupwidget.h"

class LinePasswordEdit : public QLineEdit
{
public:
    LinePasswordEdit(QWidget* parent=0);
private:
    void mousePressEvent(QMouseEvent *event);
    PopUpWidget *copied;
};

#endif // QPASSWORDEDIT_H
