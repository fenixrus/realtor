#include "popupwidget.h"
#include "ui_popupwidget.h"

PopUpWidget::PopUpWidget(QWidget *parent) :
    QWidget(parent, Qt::FramelessWindowHint | Qt::Popup),
    ui(new Ui::PopUpWidget)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_TranslucentBackground);
}

PopUpWidget::~PopUpWidget()
{
    delete ui;
}
