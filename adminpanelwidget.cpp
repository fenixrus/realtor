#include "adminpanelwidget.h"
#include "ui_adminpanelwidget.h"

AdminPanelWidget::AdminPanelWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminPanelWidget)
{
    ui->setupUi(this);
    ui->toolBox->setCurrentIndex(0);

    connect(ui->toolBox, SIGNAL(currentChanged(int)),
            this, SIGNAL(signalShowWorkers(int)));
    connect(ui->buttonAddOrEdit, SIGNAL(clicked()),
            this,SIGNAL(signalAddOrEdit()));
    connect(this,SIGNAL(signalSuccessAddOrEdit()),
            this,SLOT(slotAddedOrEdited()));
    connect(ui->buttonRemove, SIGNAL(clicked()),
            this,SIGNAL(signalRemove()));
    connect(this,SIGNAL(signalSuccessRemove()),
            this,SLOT(slotRemoved()));
    connect(ui->buttonQuery,SIGNAL(clicked()),
            this,SIGNAL(signalQueryOfData()));
    connect(ui->buttonReset,SIGNAL(clicked()),
            this,SLOT(slotReset()));


    ui->listOfWorkers->verticalHeader()->hide();
    ui->buttonRemove->setEnabled(false);
    ui->buttonQuery->setEnabled(false);
    ui->buttonAddOrEdit->setText("Добавить");
    this->styleDefaultAddOrEditButton = ui->buttonAddOrEdit->styleSheet();
    this->styleDefaultRemoveButton = ui->buttonRemove->styleSheet();
    ui->firstName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->surName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->fatherName->setValidator(new QRegExpValidator(QRegExp("[а-яА-Я]{1,30}"),this));
    ui->login->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z]{1,30}"),this));
}
AdminPanelWidget::~AdminPanelWidget()
{
    delete ui;
}

void AdminPanelWidget::setInfoIntoForm(QHash<QString, QVariant> *dataForm)
{
    ui->surName->setText(dataForm->value("surName").toString());
    ui->firstName->setText(dataForm->value("firstName").toString());
    ui->fatherName->setText(dataForm->value("fatherName").toString());
    ui->login->setText(dataForm->value("login").toString());
    ui->password->setText("(Скрыто).");
    ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
}
void AdminPanelWidget::getInfoWithForm(QHash<QString, QVariant> *dataForm)
{
    dataForm->insert("surName", ui->surName->text());
    dataForm->insert("firstName", ui->firstName->text());
    dataForm->insert("fatherName", ui->fatherName->text());
    dataForm->insert("login", ui->login->text());
    if (ui->password->text().length() == 12)
        dataForm->insert("password", ui->password->text());
    ui->password->setText("(Скрыто).");
    ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
}
void AdminPanelWidget::updateTableOfListOwnersOrObjects(QSqlQueryModel *model)
{
    ui->listOfWorkers->setModel(model);
}
void AdminPanelWidget::slotAddedOrEdited()
{
    ui->buttonAddOrEdit->setStyleSheet(this->styleDefaultAddOrEditButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForAddOrEditButton()));
}
void AdminPanelWidget::slotSetDefaultStyleForAddOrEditButton()
{
    ui->buttonAddOrEdit->setStyleSheet(this->styleDefaultAddOrEditButton);
}
void AdminPanelWidget::slotRemoved()
{
    ui->buttonRemove->setStyleSheet(this->styleDefaultRemoveButton + "background: lightgreen;");
    QTimer::singleShot(1000, this, SLOT(slotSetDefaultStyleForRemoveButton()));
}
void AdminPanelWidget::slotSetDefaultStyleForRemoveButton()
{
    ui->buttonRemove->setStyleSheet(this->styleDefaultRemoveButton);
}
void AdminPanelWidget::slotReset()
{
    ui->idOfObject->setValue(0);
    ui->surName->clear();
    ui->firstName->clear();
    ui->fatherName->clear();
    ui->login->clear();
    ui->password->setText("(Скрыто).");
    ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
}
void AdminPanelWidget::slotGenerateNewPassword()
{
    char passChars[] = {
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        '@','#','_'
    };

    srand(time(NULL)+this->countGenerateNewPasswordSymbol + rand());
    int newRand = rand() % 55;
    ui->password->setText(ui->password->text() + passChars[newRand]);

    if (++this->countGenerateNewPasswordSymbol > 11)
    {
        this->timerGenerateNewPassword->stop();
        ui->generateNewPassword->setEnabled(true);
        ui->buttonAddOrEdit->setEnabled(true);
        ui->password->setEchoMode(QLineEdit::EchoMode::Password);
    }
}
void AdminPanelWidget::on_generateNewPassword_clicked()
{
    ui->generateNewPassword->setEnabled(false);
    ui->buttonAddOrEdit->setEnabled(false);
    this->timerGenerateNewPassword = new QTimer;
    this->timerGenerateNewPassword->setInterval(90);
    connect(timerGenerateNewPassword, SIGNAL(timeout()),
            this,SLOT(slotGenerateNewPassword()));
    this->countGenerateNewPasswordSymbol=0;
    ui->password->clear();
    ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
    this->timerGenerateNewPassword->start();
}
void AdminPanelWidget::on_idOfObject_valueChanged(int arg)
{
    arg=arg;//Чтобы компилятор не плевался варнами
    if (ui->idOfObject->value() > 0)
    {
        ui->buttonRemove->setEnabled(true);
        ui->buttonQuery->setEnabled(true);
        ui->buttonAddOrEdit->setText("Изменить");
    }
    else
    {
        ui->buttonRemove->setEnabled(false);
        ui->buttonQuery->setEnabled(false);
        ui->buttonAddOrEdit->setText("Добавить");
    }
}
void AdminPanelWidget::on_toolButton_pressed()
{
    ui->password->setEchoMode(QLineEdit::EchoMode::Normal);
}
void AdminPanelWidget::on_toolButton_released()
{
    ui->password->setEchoMode(QLineEdit::EchoMode::Password);
}
int AdminPanelWidget::getIdWithForm()
{
    return ui->idOfObject->value();
}
